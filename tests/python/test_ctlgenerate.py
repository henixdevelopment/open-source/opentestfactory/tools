# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import unittest
import sys
import time

from io import StringIO
from unittest.mock import MagicMock, patch

from opentf.tools import ctlgenerate

########################################################################


class TestCtlGenerate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # exposed functions

    def test_generate_report_cmd_ok(self):
        args = ['', 'generate', 'report', '123', 'using', 'foo.doc']
        mock_gr = MagicMock()
        mock_eo = MagicMock(return_value='Wfff')
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlgenerate._ensure_options', mock_eo
        ), patch('opentf.tools.ctlgenerate.read_configuration'), patch(
            'opentf.tools.ctlgenerate._is_command', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlgenerate.generate_report', mock_gr
        ):
            ctlgenerate.generate_report_cmd()
        mock_eo.assert_called_once()
        mock_gr.assert_called_once_with('Wfff', 'foo.doc')

    def test_generate_report_cmd_ko_unknown_command(self):
        args = ['', 'rrr']
        mock_error = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlgenerate._is_command', MagicMock(return_value=False)
        ), patch('opentf.tools.ctlgenerate._error', mock_error):
            self.assertRaisesRegex(SystemExit, '1', ctlgenerate.generate_report_cmd)
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )

    def test_print_generate_report_help_ok(self):
        args = ['', 'rgrgrg']
        mock_print = MagicMock()
        with patch(
            'opentf.tools.ctlgenerate._is_command', MagicMock(return_value=True)
        ), patch('builtins.print', mock_print):
            ctlgenerate.print_generate_report_help(args)
        mock_print.assert_called_once()

    def test_print_generate_report_help_ko_unknown_command(self):
        args = ['', 'rgrgrg']
        mock_print = MagicMock()
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctlgenerate._is_command', MagicMock(return_value=False)
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.print_generate_report_help, args
            )
        mock_print.assert_not_called()
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )

    # generate_report

    def test_generate_report_ok(self):
        mock_arg = MagicMock(return_value=None)
        mock_collector = MagicMock()
        with patch('os.path.isfile', MagicMock(return_value=False)), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlgenerate._query_insightcollector', mock_collector
        ), patch(
            'opentf.tools.ctlgenerate._get_arg', mock_arg
        ):
            ctlgenerate.generate_report('Id', 'file.ext')
        mock_collector.assert_called_once_with('Id', '*', 'file.ext', '.', None, 60.0)

    def test_generate_report_ko_missing_using(self):
        mock_arg = MagicMock(side_effect=[None, None, None, None, None])
        mock_error = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_arg', mock_arg), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.generate_report, 'IdA', None
            )
        mock_error.assert_called_once_with('Configuration file path not provided.')

    def test_generate_report_ko_path_is_file(self):
        mock_arg = MagicMock(side_effect=[None, 'file.path', None, None, None])
        mock_error = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_arg', mock_arg), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ), patch('os.path.isfile', MagicMock(return_value=True)):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.generate_report, 'IdB', 'some.file'
            )
        mock_error.assert_called_once()
        self.assertEqual('file.path', mock_error.call_args[0][1])

    def test_generate_report_ko_config_file_not_found(self):
        mock_arg = MagicMock(side_effect=[None, 'file.path', None, None, None])
        mock_error = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_arg', mock_arg), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ), patch('os.path.isfile', MagicMock(return_value=False)), patch(
            'os.path.exists', MagicMock(return_value=False)
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.generate_report, 'IdC', 'zzz.exe'
            )
        mock_error.assert_called_once_with(
            'Configuration file `%s` not found.', 'zzz.exe'
        )

    def test_generate_report_ko_exception(self):
        mock_arg = MagicMock(return_value=None)
        mock_collector = MagicMock(side_effect=Exception('Boo.'))
        mock_fatal = MagicMock()
        with patch('os.path.isfile', MagicMock(return_value=False)), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlgenerate._query_insightcollector', mock_collector
        ), patch(
            'opentf.tools.ctlgenerate._get_arg', mock_arg
        ), patch(
            'opentf.tools.ctlgenerate._fatal', mock_fatal
        ):
            ctlgenerate.generate_report('Id', 'bar.mp3')
        mock_collector.assert_called_once_with('Id', '*', 'bar.mp3', '.', None, 60.0)
        mock_fatal.assert_called_once()
        self.assertEqual('Boo.', mock_fatal.call_args[0][-1])

    def test_generate_report_ko_save_as_and_filename_pattern(self):
        mock_arg = MagicMock(side_effect=['pattern*', '.', 'filename', '8'])
        mock_error = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_arg', mock_arg), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.generate_report, 'IdC', 'zzz.exe'
            )
        mock_error.assert_called_once_with(
            'Cannot download multiple reports using the same file name (`%s`).',
            'filename',
        )

    def test_generate_report_ko_save_as_is_a_path(self):
        mock_arg = MagicMock(side_effect=['not_pattern', '.', '/dir/filename', '8'])
        mock_error = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_arg', mock_arg), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctlgenerate.generate_report, 'IdC', 'zzz.exe'
            )
        mock_error.assert_called_once_with(
            'File name `%s` is a path. Use `--save-to` option to specify the download directory.',
            '/dir/filename',
        )

    # query_insightcollector

    def test_query_insightcollector_ok(self):
        wr = [
            {
                'metadata': {
                    'attachments': {'a': {'uuid': 'uuid_1'}, 'b': {'uuid': 'uuid_2'}}
                }
            }
        ]
        mock_download = MagicMock()
        with patch(
            'opentf.tools.ctlgenerate._ensure_uuid', MagicMock(return_value='IdF')
        ), patch(
            'opentf.tools.ctlgenerate._post_insight_definition',
            MagicMock(return_value='Wroom'),
        ), patch(
            'opentf.tools.ctlgenerate._process_insightcollector_result',
            MagicMock(return_value=wr),
        ), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlgenerate.download_attachment', mock_download
        ), patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ):
            ctlgenerate._query_insightcollector(
                'IdF', 'pattern', 'file.path', 'download.path', '', '8'
            )
        self.assertEqual(2, mock_download.call_count)
        self.assertEqual(
            ('IdF', 'uuid_1', 'download.path', ''), mock_download.call_args_list[0][0]
        )

    def test_query_insightcollector_ok_make_dir_from_path(self):
        wr = [
            {
                'metadata': {
                    'attachments': {'a': {'uuid': 'uuid_3'}, 'b': {'uuid': 'uuid_4'}}
                }
            }
        ]
        mock_download = MagicMock()
        mock_dirs = MagicMock()
        with patch(
            'opentf.tools.ctlgenerate._ensure_uuid', MagicMock(return_value='IdG')
        ), patch(
            'opentf.tools.ctlgenerate._post_insight_definition',
            MagicMock(return_value='Wroom'),
        ), patch(
            'opentf.tools.ctlgenerate._process_insightcollector_result',
            MagicMock(return_value=wr),
        ), patch(
            'os.path.exists', MagicMock(return_value=False)
        ), patch(
            'os.makedirs', mock_dirs
        ), patch(
            'opentf.tools.ctlgenerate.download_attachment', mock_download
        ), patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ):
            ctlgenerate._query_insightcollector(
                'IdG', 'pattern', 'file.path', 'download.path', '', '8'
            )
        self.assertEqual(2, mock_download.call_count)
        self.assertEqual(
            ('IdG', 'uuid_4', 'download.path', ''), mock_download.call_args_list[1][0]
        )
        mock_dirs.assert_called_once_with('download.path')

    def test_query_insightcollector_ko_timeout(self):
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctlgenerate._ensure_uuid', MagicMock(return_value='IdH')
        ), patch(
            'opentf.tools.ctlgenerate._post_insight_definition',
            MagicMock(return_value='Wroom'),
        ), patch(
            'opentf.tools.ctlgenerate._process_insightcollector_result',
            MagicMock(return_value=[]),
        ), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlgenerate._error', mock_error
        ), patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ):
            self.assertRaisesRegex(
                SystemExit,
                '1',
                ctlgenerate._query_insightcollector,
                'IdH',
                'pattern',
                'file.path',
                'download.path',
                '',
                '8',
            )
        mock_error.assert_called_once_with(
            'Timeout while retrieving generated reports.'
        )

    # process_insightcollector_result

    def test_process_insightcollector_result_ok(self):
        result = {
            'code': 200,
            'details': {
                'expected': [('report', 'SummaryReport')],
                'request_id': 'RiD1',
            },
        }
        mock_get_results = MagicMock()
        with patch('opentf.tools.ctlgenerate._get_workflow_results', mock_get_results):
            ctlgenerate._process_insightcollector_result(
                result, 'wFiD', 'config.path', 'name.pattern', 8.0
            )
        mock_get_results.assert_called_once()
        self.assertEqual(1, mock_get_results.call_args[0][0])
        self.assertEqual('RiD1', mock_get_results.call_args[0][1])

    def test_process_insightcollector_result_ko_code_422(self):
        result = {'code': 422, 'message': 'O_O'}
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('opentf.tools.ctlgenerate._fatal', mock_fatal):
            self.assertRaises(
                SystemExit,
                ctlgenerate._process_insightcollector_result,
                result,
                'wFiD',
                'config.path',
                'name.pattern',
                8.0,
            )
        mock_fatal.assert_called_once()
        self.assertEqual('O_O', mock_fatal.call_args[0][2])

    def test_process_insightcollector_result_ko_unexpected_response(self):
        result = {
            'code': 200,
            'not_details': {
                'expected': [('report', 'SummaryReport')],
                'request_id': 'RiD2',
            },
        }
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('opentf.tools.ctlgenerate._fatal', mock_fatal):
            self.assertRaises(
                SystemExit,
                ctlgenerate._process_insightcollector_result,
                result,
                'wFiD',
                'config.path',
                'name.pattern',
                8.0,
            )
        mock_fatal.assert_called_once()
        self.assertIn('not_details', mock_fatal.call_args[0][1])

    def test_process_insightcollector_result_ok_zero_expected_reports(self):
        result = {'code': 200, 'details': {'expected': [], 'request_id': 'RiD3'}}
        mock_warning = MagicMock()
        with patch('opentf.tools.ctlgenerate._warning', mock_warning):
            self.assertRaisesRegex(
                SystemExit,
                '0',
                ctlgenerate._process_insightcollector_result,
                result,
                'wFiD',
                'config.path',
                'name.pattern',
                8.0,
            )
        mock_warning.assert_called_once()
        self.assertIn(
            'No expected reports can be generated', mock_warning.call_args[0][0]
        )

    def test_process_insightcollector_result_ko_no_request_id(self):
        result = {
            'code': 200,
            'details': {'expected': [('report', 'SummaryReport')], 'request_id': None},
        }
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('opentf.tools.ctlgenerate._fatal', mock_fatal):
            self.assertRaises(
                SystemExit,
                ctlgenerate._process_insightcollector_result,
                result,
                'wFiD',
                'config.path',
                'name.pattern',
                8.0,
            )
        mock_fatal.assert_called_once()
        self.assertIn('request id parameter missing', mock_fatal.call_args[0][0])

    # get_workflow_results

    def test_get_workflow_results_ok(self):
        results = [
            {'kind': 'WorkflowResult', 'metadata': {'request_id': '112'}},
            {'kind': 'WorkflowResult', 'metadata': {'request_id': '112'}},
        ]
        mock_events = MagicMock(side_effect=[[], results])
        with patch('time.sleep'), patch(
            'opentf.tools.ctlgenerate._get_workflow_events', mock_events
        ), patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ):
            result = ctlgenerate._get_workflow_results(
                2, '112', 'wFiD', time.time() + 60
            )
        self.assertEqual(2, mock_events.call_count)
        self.assertEqual(2, len(result))

    def test_get_workflow_results_timeout(self):
        mock_events = MagicMock(side_effect=[[], [], []])
        mock_time = MagicMock(side_effect=[10, 55, 1000])
        with patch('time.sleep'), patch('time.time', mock_time), patch(
            'opentf.tools.ctlgenerate._get_workflow_events', mock_events
        ), patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ):
            result = ctlgenerate._get_workflow_results(2, '223', 'wfId', 60.0)
        self.assertEqual(1, mock_events.call_count)
        self.assertEqual([], result)

    def test_get_workflow_results_no_insights_published(self):
        results = [
            {
                'kind': 'WorkflowResult',
                'metadata': {'name': 'No insights published', 'request_id': '112'},
            }
        ]
        mock_events = MagicMock(side_effect=[results])
        with patch('time.sleep'), patch(
            'opentf.tools.ctlgenerate._get_workflow_events', mock_events
        ):
            result = ctlgenerate._get_workflow_results(
                2, '112', 'wFiD', time.time() + 60
            )
        self.assertEqual(1, mock_events.call_count)
        self.assertEqual(1, len(result))

    # post_insight_definition

    def test_post_insight_definition_ok(self):
        mock_open = MagicMock(return_value={})
        mock_post = MagicMock()
        with patch('builtins.open', mock_open), patch(
            'opentf.tools.ctlgenerate._post', mock_post
        ), patch('opentf.tools.ctlgenerate._insightcollector'):
            ctlgenerate._post_insight_definition('wFid', 'config.path', 'pattern')
        mock_open.assert_called_once_with('config.path', 'rb')
        mock_post.assert_called_once()
        self.assertEqual(
            '/workflows/wFid/insights?insight=pattern', mock_post.call_args[0][1]
        )

    def test_post_insight_definition_ko_file_not_found(self):
        mock_open = MagicMock(side_effect=FileNotFoundError('Wroom'))
        mock_not_found = MagicMock()
        with patch('builtins.open', mock_open), patch(
            'opentf.tools.ctlgenerate._file_not_found', mock_not_found
        ):
            ctlgenerate._post_insight_definition('ff', 'config.path', 'pattern')
        mock_open.assert_called_once()
        mock_not_found.assert_called_once()

    def test_post_insight_definition_ko_exception(self):
        mock_open = MagicMock(side_effect=Exception('Hum'))
        mock_fatal = MagicMock()
        with patch('builtins.open', mock_open), patch(
            'opentf.tools.ctlgenerate._fatal', mock_fatal
        ):
            ctlgenerate._post_insight_definition('gg', 'config.path', 'pattern')
        mock_open.assert_called_once()
        mock_fatal.assert_called_once()
        self.assertEqual('Hum', mock_fatal.call_args[0][1])

    # _get_timeout

    def test_get_timeout_ve(self):
        with patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ), patch('opentf.tools.ctlgenerate._warning') as mock_warn:
            res = ctlgenerate._get_timeout('-1234')
        mock_warn.assert_called_once()
        self.assertEqual(ctlgenerate.DEFAULT_QUERY_TIMEOUT, res)

    def test_get_timeout_ko_polling_delay(self):
        with patch(
            'opentf.tools.ctlgenerate.CONFIG', {'orchestrator': {'polling-delay': 5}}
        ), patch('opentf.tools.ctlgenerate._warning') as mock_warn:
            res = ctlgenerate._get_timeout('2')
        mock_warn.assert_called_once()
        self.assertEqual(2.0, res)
