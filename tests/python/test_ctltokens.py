# Copyright (c) 2022-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch, mock_open

import cryptography
import jwt

import opentf.tools.ctltokens


########################################################################
# Datasets

TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJleGFtcGxlLmNvbSIsInN1YiI6Im5vYm9keSJ9.ABsPbmeakwzxS-brB8BZcqMMonqolTCG6PJwcxACSMkl7lLURocEXzybLxhhlarQuCFt3wpZpEeDRoXjlLketl-ZruzQQ889730KD1m1VvBBzwpulrrv9uWLY9TVjnC7r-j3Rhf758HDCpozFy-Q6fFP1mgDL_ZqVZVFOUWkQeI-dzwebzLfsz7Y_KZsuwTvimpjzQ8GePMwjp0sPVmEAuC8GDPiUFA2WbHJGrS1LQGSD5T9j8QsXnv_uaVpag5KFzfN0ZG0jFZe4UBiPi7Grq_WjAc8lA9GrAduV5m7SP8jl4DMA0ufeRAFLzdlg53qRPp8o2Lr1IjX_qBxp6ZrpIS_bjbkI41WEaHNreFK6Zh2eqz-Ms60RphYQw_hDSqlwc402WLGvwCururKCzDelKAXSS0HFlWPS4cY7wHDJmRy76XOYNrsxoAAg__Ci1FspzWw8ngNvkMnwuQpRrYsOhQ4W0orNTpYtQWXdt8HKm2fb-lXe5Rmxev9lZnqKe77HNET8ITKPz7pC_ZVCcV4GneMY3ryzUAMWMM2bBzTkjvWSCMxSCMa7vptL-nCSZro8H1GYzWY9PAW0ZU5Mj7-iYO1A0ciHnDkJziQNRTksD30qt45se1ot8G4Kt8P6vxxsi-mgAjYnZGOH_g5EW54q806cMq4vJ-DLn-Llov3Y3s'

TOKEN_WITH_EXP = 'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJoZW5peCIsInN1YiI6Im1sZiIsImV4cCI6MTY3MDEwODQwMH0.znUyjEuHJmITqmajzDqWeTU3p2DyVIJyFnxArHkr8EUv90QzaTahw7X6Dy9VknFB-dvRGv41uO0jcuOmJETomNnNU9wPq2slELimyoqKRHpKMWgfT2dD2yyUI1d0kGhHsYelM4YMx71OJQbv7dr5BpMA7iwr6MEbGv0NIM4BOh5wpzTrNfEJgBCZp62K5UKUbtJVsFDtevSzujYTentcl3YyYryLwHOkRCGWEH2g3S82-OqeUY4eCvCri8FCn6a9VEZajXYYjEUWcxRaNKOH_di8XNCsTI0rAIVWFV1TCBgkOuDn4krNSPGyU54WImbbFiGcuTV_G0sV8z9Vfr1lSj4ddcktpaq6B3n4cx_AA2z7y2TCruUgT4LNUZRn-zZjuL693X2XKdlZqhCgvlKK1v1MNQQtK6Avf3v1q_uL-6enqVSKSQvHyfkjZF2NAOlBUpbiNY2EhuXY94pHqjkdpnAkM7S-uPlaPZXeIhV3l7KRtoKSig4LUEdRz3HnJA0cvalvxu5Ld8w5N0brqmTG71Pp60gBAUi-lW7cb1e35zwgSZNg6SQ0inbr8Dgk50nVJZrxcGH37zI68AQ62tIq5yPO25D0OzvnoYT4ZQ7mZJcctmRuT8oauzmyV-0vmxgR8h6_hBTBFxj5K8reyWPQlgOs-3bARnLkyKGBUOJycAU'

########################################################################
# Helpers


def _generate_token():
    """Generate temporary key and JWT token."""
    # pylint: disable=import-outside-toplevel
    from cryptography.hazmat.primitives import serialization
    from cryptography.hazmat.primitives.asymmetric import rsa
    from cryptography.hazmat.backends import default_backend

    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=4096, backend=default_backend()
    )
    public_key = private_key.public_key()
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    ).decode()
    pub = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    token = jwt.encode(
        {'iss': 'check_token_unittest', 'sub': 'temp token'},
        pem,
        algorithm='RS512',
    )
    return token, str(pub, encoding='utf-8')


class TestCtlTokens(unittest.TestCase):
    """Unit tests for ctltokens."""

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # main

    def test_main_generate_token(self):
        key_id = 'kEy_Id'
        args = ['', 'generate', 'token', 'using', key_id]
        mock_generatetoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.generate_token', mock_generatetoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_generatetoken.assert_called_once_with(key_id)

    def test_main_generate_token_unexpected_arg(self):
        key_id = 'kEy_Id'
        args = ['', 'generate', 'token', 'using', key_id, 'aaa']
        mock_generatetoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.generate_token', mock_generatetoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_generatetoken.assert_not_called()

    def test_main_view_token(self):
        token = 'tOKen'
        args = ['', 'view', 'token', token]
        mock_viewtoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.view_token', mock_viewtoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_viewtoken.assert_called_once_with(token)

    def test_main_view_token_unexpected_arg(self):
        token = 'tOKen'
        args = ['', 'view', 'token', token, 'fOO']
        mock_viewtoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.view_token', mock_viewtoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_viewtoken.assert_not_called()

    def test_main_check_token(self):
        key_id = 'kEy_Id'
        args = ['', 'check', 'token', 'tOKen', 'using', key_id]
        mock_checktoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.check_token', mock_checktoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_checktoken.assert_called_once_with('tOKen', key_id)

    def test_main_check_token_unexpected_arg(self):
        key_id = 'kEy_Id'
        args = ['', 'check', 'token', 'tOKen', 'using', key_id, 'fOO']
        mock_checktoken = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctltokens.check_token', mock_checktoken
        ):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.tokens_cmd)
        mock_checktoken.assert_not_called()

    # print_help

    def test_print_help_generate_token(self):
        args = ['', 'generate', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctltokens.print_tokens_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_view_token(self):
        args = ['', 'view', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctltokens.print_tokens_help(args)
        mock_print.assert_called_once()

    def test_print_help_check_token(self):
        args = ['', 'check', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctltokens.print_tokens_help(args)
        mock_print.assert_called_once()

    # tokens

    def test_load_pem_private_key_nofile(self):
        self.assertRaises(
            SystemExit, opentf.tools.ctltokens._load_pem_private_key, 'aKey'
        )

    def test_load_pem_private_key_notaprivatekey(self):
        with patch('builtins.open', mock_open(read_data=b'nOTaPrivateKey')), patch(
            'builtins.input'
        ):
            self.assertRaises(
                SystemExit, opentf.tools.ctltokens._load_pem_private_key, 'aKey'
            )

    def test_load_pem_private_key_encryptedprivatekey(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch('builtins.open', mock_open(read_data=b'nOTaPrivateKey')), patch(
            'getpass.getpass', MagicMock(side_effect=ValueError('oh no'))
        ), patch(
            'cryptography.hazmat.primitives.serialization.load_pem_private_key',
            MagicMock(side_effect=TypeError('Oops')),
        ), patch(
            'opentf.tools.ctltokens._fatal', mock_fatal
        ):
            self.assertRaises(
                SystemExit, opentf.tools.ctltokens._load_pem_private_key, 'aKey'
            )
        mock_fatal.assert_called_once_with('oh no')

    def test_load_pem_private_key_encryptedprivatekeynopassphrase(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch('builtins.open', mock_open(read_data=b'nOTaPrivateKey')), patch(
            'getpass.getpass', MagicMock(return_value='')
        ), patch(
            'cryptography.hazmat.primitives.serialization.load_pem_private_key',
            MagicMock(side_effect=TypeError('Oops')),
        ), patch(
            'opentf.tools.ctltokens._fatal', mock_fatal
        ):
            self.assertRaises(
                SystemExit, opentf.tools.ctltokens._load_pem_private_key, 'aKey'
            )
        mock_fatal.assert_called_once_with(
            'Passphrase cannot be empty, the private key is encrypted.'
        )

    def test_load_pem_private_key_unsupportedalgorithm(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch('builtins.open', mock_open(read_data=b'nOTaPrivateKey')), patch(
            'cryptography.hazmat.primitives.serialization.load_pem_private_key',
            MagicMock(side_effect=cryptography.exceptions.UnsupportedAlgorithm('Oops')),
        ), patch('opentf.tools.ctltokens._fatal', mock_fatal):
            self.assertRaises(
                SystemExit, opentf.tools.ctltokens._load_pem_private_key, 'aKey'
            )
        mock_fatal.assert_called_once()
        self.assertEqual(
            mock_fatal.call_args[0][0],
            'The serialized key type is not supported by the OpenSSL version "cryptography" is using: %s.',
        )

    def test_load_pem_private_key_encrypted_ok(self):
        def _mock_loadpemprivatekey(_, pwd, backend=None):
            if pwd:
                return 'pk with passphrase'
            raise TypeError

        with patch('builtins.open', mock_open(read_data=b'nOTaPrivateKey')), patch(
            'cryptography.hazmat.primitives.serialization.load_pem_private_key',
            _mock_loadpemprivatekey,
        ), patch('getpass.getpass') as mock_getpass:
            what = opentf.tools.ctltokens._load_pem_private_key('aKey')
        self.assertEqual(what, 'pk with passphrase')
        mock_getpass.assert_called_once()

    def test_generate_token_badexp(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        mock_input = MagicMock(return_value='foobar')
        with patch('opentf.tools.ctltokens._load_pem_private_key'), patch(
            'builtins.input', mock_input
        ), patch('opentf.tools.ctltokens._fatal', mock_fatal):
            self.assertRaises(SystemExit, opentf.tools.ctltokens.generate_token, None)
        mock_fatal.assert_called_once_with(
            'Invalid expiration date format, must be YYYY/MM/DD.'
        )

    def test_generate_token_gooddexp(self):
        mock_jwt = MagicMock()
        mock_jwt.encode = MagicMock()
        mock_input = MagicMock(return_value='2023/01/01')
        with patch('opentf.tools.ctltokens._load_pem_private_key'), patch(
            'builtins.input', mock_input
        ), patch('opentf.tools.ctltokens.jwt', mock_jwt):
            opentf.tools.ctltokens.generate_token('foobar')
        mock_jwt.encode.assert_called_once()

    def test_generate_token_outputfile(self):
        args = ['', '--output-file', 'foobar']
        mock_jwt = MagicMock()
        mock_jwt.encode = MagicMock()
        mock_input = MagicMock(return_value='2023/01/01')
        with patch('opentf.tools.ctltokens._load_pem_private_key'), patch(
            'builtins.input', mock_input
        ), patch('opentf.tools.ctltokens.jwt', mock_jwt), patch(
            'sys.argv', args
        ), patch(
            'builtins.open'
        ) as mock_open:
            opentf.tools.ctltokens.generate_token('foobar')
        mock_jwt.encode.assert_called_once()
        mock_open.assert_called_once()

    def test_view_token_ok(self):
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            self.assertIsNone(opentf.tools.ctltokens.view_token(TOKEN))
        mock_print.assert_called_with({'iss': 'example.com', 'sub': 'nobody'})

    def test_view_token_withexp_ok(self):
        mock_print = MagicMock()
        mock_fromtimestamp = MagicMock()
        mock_fromtimestamp.strftime = MagicMock(return_value='aDate')
        mock_datetime = MagicMock()
        mock_datetime.fromtimestamp = MagicMock(return_value=mock_fromtimestamp)
        with patch('builtins.print', mock_print), patch(
            'opentf.tools.ctltokens.datetime', mock_datetime
        ):
            self.assertIsNone(opentf.tools.ctltokens.view_token(TOKEN_WITH_EXP))
        mock_print.assert_called_with({'iss': 'henix', 'sub': 'mlf', 'exp': 'aDate'})
        mock_datetime.fromtimestamp.assert_called_once()
        mock_fromtimestamp.strftime.assert_called_once_with('%Y/%m/%d')

    def test_view_token_invalid(self):
        self.assertRaises(SystemExit, opentf.tools.ctltokens.view_token, 'bAD')

    def test_check_token_nopublickey(self):
        self.assertRaises(SystemExit, opentf.tools.ctltokens.check_token, TOKEN, '')

    def test_check_token_ok_publickey(self):
        token, pub = _generate_token()
        with patch('builtins.open', mock_open(read_data=pub)):
            response = opentf.tools.ctltokens.check_token(token, 'pUbliCKey')
        self.assertIsNone(response)

    def test_check_token_bad_publickey(self):
        token, _ = _generate_token()
        with patch('builtins.open', mock_open(read_data='nOTaPublicKey')):
            self.assertRaises(
                SystemExit, opentf.tools.ctltokens.check_token, token, 'pUbliCKey'
            )
