# Copyright (c) 2022-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch, mock_open

from requests import Response, exceptions

from opentf.tools.ctlworkflows import (
    workflow_cmd,
    print_workflow_help,
    _add_files,
    _add_variables,
    _add_tags,
    _filter_workflows,
    _get_outputformat,
    _get_first_page,
    _get_workflow_events,
    _emit_command,
    delete_workflow,
    list_workflows,
    run_workflow,
    get_workflow,
    _handle_maybe_details,
    _apply_qualitygate,
    _download_execution_reports,
)


########################################################################
# Datasets

CONFIG = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'max-retry': 1,
        'polling-delay': 1,
        'warmup-delay': 1,
        'services': {
            'eventbus': {'port': 1234},
            'receptionist': {'port': 4321, 'prefix': 'reCeptioNist'},
            'observer': {'port': 9999, 'force-base-url': True},
            'killswitch': {'port': 8888},
        },
        'server': 'http://aaaa',
    },
}

CONFIG_LEGACY = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'max-retry': 1,
        'polling-delay': 1,
        'warmup-delay': 1,
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'http://aaaa',
    },
}


HEADERS = {'Authorization': 'Bearer ' + CONFIG_LEGACY['token']}

OBSERVER_WORKFLOWS_EMPTY = {'details': {'items': []}}
OBSERVER_WORKFLOWS_EMPTY_DONE = {'details': {'items': [], 'status': 'DONE'}}
OBSERVER_WORKFLOWS_EMPTY_RUNNING = {'details': {'items': [], 'status': 'RUNNING'}}
OBSERVER_WORKFLOWS = {'details': {'items': ['a1', 'b2']}}

OBSERVER_FIRST_PAGE_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'DONE',
    },
}

OBSERVER_FIRST_PAGE_RUNNING_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_DONE_OK = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_SECOND_PAGE_OK = {
    'details': {
        'items': [
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_FIRST_PAGE_FAILED = {
    'details': {
        'items': [
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            }
        ],
        'status': 'FAILED',
    },
}

OBSERVER_FIRST_PAGE_SPURIOUS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionerror_job_id',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_NOTIFICATIONS_SPURIOUS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'Notification',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'spec': {
                    'logs': ['foo', '[DEBUG] foo'],
                },
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionerror_job_id',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_FIRST_PAGE_SPURIOUS_NOCTS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_2_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': ['foo'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
            },
            {
                'kind': 'ExecutionError',
                'metadata': {
                    'name': 'nAMe',
                    'workflow_id': 'abc-id',
                },
                'details': {
                    'foo.bar': 'FOO.bAR',
                    'bar.baZ': 'BAR.BAz',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe3',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -2,
                    'job_id': 'executioncommand_3_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe_release',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                },
                'status': 456,
                'logs': ['executionresult_3_log'],
            },
        ],
        'status': 'RUNNING',
    },
}

OBSERVER_ATTACHMENTS_OK = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe',
                    'creationTimestamp': '1970-01-23',
                    'step_sequence_id': -1,
                    'job_id': 'executioncommand_job_id',
                },
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_job_id',
                },
                'status': 0,
                'logs': ['executionresult_log'],
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
                'step': {
                    'id': 'some_provider_command_42',
                    'with': {'test': 'some test case'},
                },
                'scripts': [],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_id': 32,
                    'step_sequence_id': 43,
                    'job_id': 'executioncommand_2_job_id',
                },
                'scripts': [],
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'attachments': {
                        '/tmp/cb3d3401-103e-40a0-ba9a-1004da105c68-52985638-f20b-4b08-880f-e538af8b2f1c_66_output.xml': {
                            'uuid': '5b444910-a246-4d66-b02b-40e77148b90d'
                        }
                    },
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                    'step_id': 32,
                    'step_origin': ['some_provider_command_42'],
                },
                'status': 0,
                'logs': ['executionresult_2_log'],
                'attachments': [
                    '/tmp/cb3d3401-103e-40a0-ba9a-1004da105c68-52985638-f20b-4b08-880f-e538af8b2f1c_66_output.xml'
                ],
            },
            {
                'kind': 'WorkflowResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'workflowresult_1_job_id',
                    'attachments': {
                        '/tmp/b10a8635-0d36-4f40-8776-d9ac741e1b0f-af0edfc3-6cc6-4a25-912b-e11d1d09b752_WR_executionlog.txt': {
                            'type': 'tYpe',
                            'uuid': 'af0edfc3-6cc6-4a25-912b-e11d1d09b752',
                        }
                    },
                    'workflow_id': 'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
                },
                'attachments': [
                    '/tmp/b10a8635-0d36-4f40-8776-d9ac741e1b0f-af0edfc3-6cc6-4a25-912b-e11d1d09b752_WR_executionlog.txt'
                ],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_ALLURE = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'WorkflowResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'workflowresult_1_job_id',
                    'workflow_id': 'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
                },
                'attachments': [
                    '/tmp/allureReporting/627e5c92-0f9d-4644-96fc-b4f52f84bedc/allure-report.tar'
                ],
            },
        ],
        'status': 'DONE',
    },
}

OBSERVER_NOT_USEFUL_ATTACHMENTS = {
    'details': {
        'items': [
            {},
            {
                'kind': 'Workflow',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                },
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'providercommand_job_id',
                },
                'step': {
                    'id': 'some_provider_command_42',
                    'with': {'test': 'some test case'},
                },
                'scripts': [],
            },
            {
                'kind': 'ExecutionCommand',
                'metadata': {
                    'name': 'executioncommand_nAMe2',
                    'creationTimestamp': '1970-01-23',
                    'step_id': 32,
                    'step_sequence_id': 43,
                    'job_id': 'executioncommand_2_job_id',
                },
                'scripts': [],
                'runs-on': ['windows'],
            },
            {
                'kind': 'ExecutionResult',
                'metadata': {
                    'attachments': {
                        '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html': {
                            'uuid': '5b444910-a246-4d66-b02b-40e77148b90d'
                        }
                    },
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'executionresults_2_job_id',
                    'step_id': 32,
                    'step_origin': ['some_provider_command_42'],
                },
                'status': 123,
                'logs': ['executionresult_2_log'],
                'attachments': [
                    '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html'
                ],
            },
            {
                'kind': 'WorkflowResult',
                'metadata': {
                    'name': 'nAMe',
                    'creationTimestamp': '1970-01-23',
                    'job_id': 'workflowresult_1_job_id',
                    'workflow_id': 'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
                },
                'attachments': [
                    '/tmp/allureReporting/627e5c92-0f9d-4644-96fc-b4f52f84bedc/allure-report.tar'
                ],
            },
        ],
        'status': 'DONE',
    },
}


UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_BAD = 'z6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


class TestCtlWorkflows(unittest.TestCase):
    """Unit tests for ctlworkflows."""

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    ## ctlworkflows

    # workflow_cmd

    def test_workflow_cmd_invalid(self):
        args = ['', 'workflow', 'unknown']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_get_workflows(self):
        args = ['', 'get', 'workflows']
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(return_value=OBSERVER_WORKFLOWS_EMPTY)
        mock_get = MagicMock(return_value=mockresponse)
        mock2 = MagicMock()
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock2
        ):
            workflow_cmd()
        mock_get.assert_called_once()
        self.assertEqual(mock_get.call_args[0][0], 'http://aaaa:9999')
        mock2.assert_called_once()

    def test_workflow_cmd_kill_workflow_bad(self):
        args = ['', 'kill', 'workflow', UUID_BAD, '--unknown']
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_get_workflow_unknown_option(self):
        args = ['', 'get', 'workflow', UUID_OK, '--wait']
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_run_workflow_incomplete_envvar(self):
        args = ['', 'run', 'workflow', 'yada', '-e']
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, workflow_cmd)

    def test_workflow_cmd_get_workflows_legacyconfig(self):
        args = ['', 'get', 'workflows']
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(return_value=OBSERVER_WORKFLOWS_EMPTY)
        mock_get = MagicMock(return_value=mockresponse)
        mock_readconfig = MagicMock()
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.read_configuration', mock_readconfig
        ):
            workflow_cmd()
        mock_get.assert_called_once()
        self.assertEqual(mock_get.call_args[0][0], 'http://aaaa:9999')
        mock_readconfig.assert_called_once()

    # print_workflow_help

    def test_print_workflow_help_unknown(self):
        args = ['', 'unknown']
        self.assertRaises(SystemExit, print_workflow_help, args)

    def test_print_workflow_help_kill_workflow(self):
        args = ['', 'kill', 'workflow', '123']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            print_workflow_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_workflow_help_get_workflows(self):
        args = ['', 'get', 'workflows']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            print_workflow_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_workflow_help_run_workflow(self):
        args = ['', 'run', 'workflow']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            print_workflow_help(args)
        self.assertTrue(mock_print.call_count > 0)

    # _add_tags

    def test_add_tags_badtagsyntax(self):
        args = ['', 'run', 'workflow', '--tags=*:,a']
        with patch('builtins.print'), patch('sys.argv', args):
            self.assertRaises(SystemExit, _add_tags, None, {})

    def test_add_tags_badtagmultios(self):
        args = ['', 'run', 'workflow', '--tags=windows,linux']
        with patch('builtins.print'), patch('sys.argv', args):
            self.assertRaises(SystemExit, _add_tags, None, {})

    def test_add_tags_replaceos(self):
        wf = '{"metadata": {"name": "aa"}, "jobs": {"job-a": {"runs-on": "linux"}}}'
        files = {'workflow': wf}
        args = ['', 'run', 'workflow', '--tags=windows']
        with patch('builtins.print'), patch('sys.argv', args):
            _add_tags([], files)
        workflow = files['workflow'].read()
        self.assertIn('windows', workflow)
        self.assertNotIn('linux', workflow)

    def test_add_tags_newtag(self):
        wf = '{"metadata": {"name": "aa"}, "jobs": {"job-a": {"runs-on": "linux"}}}'
        files = {'workflow': wf}
        args = ['', 'run', 'workflow', '--tags=foobar']
        with patch('builtins.print'), patch('sys.argv', args):
            _add_tags([], files)
        workflow = files['workflow'].read()
        self.assertIn('linux', workflow)
        self.assertIn('foobar', workflow)

    # _add_files

    def test_add_files_long_oneenv(self):
        args = [
            'foo=bar',
            '-f',
            'yada=/foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e',
            'nono=noway',
        ]
        files = {}
        with patch('builtins.open', lambda x, y: 'DUMMY'):
            _add_files(args, files)
        self.assertEqual(len(files), 2)
        self.assertTrue('yada' in files)
        self.assertTrue('yoda' in files)

    def test_add_files_short_oneenv(self):
        args = [
            'foo=bar',
            '-f=yada=/foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e=nono=noway',
        ]
        files = {}
        with patch('builtins.open', lambda x, y: 'DUMMY'):
            _add_files(args, files)
        self.assertEqual(len(files), 2)
        self.assertTrue('yada' in files)
        self.assertTrue('yoda' in files)

    # _add_variables

    def test_add_variables_long_onefile(self):
        args = [
            'foo=bar',
            '-e',
            'yada=foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e',
            'nono=noway',
        ]
        files = {}
        _add_variables(args, files)
        self.assertEqual(len(files), 1)
        self.assertIn('variables', files)
        self.assertIn('yada=foo/bar', files['variables'])
        self.assertNotIn('yoda=/bar/baz', files['variables'])

    def test_add_variables_short_onefile(self):
        args = [
            'foo=bar',
            '-e=yada=foo/bar',
            '-f',
            'yoda=/bar/baz',
            '-e',
            'nono=noway',
        ]
        files = {}
        _add_variables(args, files)
        self.assertEqual(len(files), 1)
        self.assertIn('variables', files)
        self.assertIn('yada=foo/bar', files['variables'])
        self.assertNotIn('yoda=/bar/baz', files['variables'])

    def test_add_variables_readfromfile_mixed(self):
        args = ['foo=bar', '-e', 'yada=foo/bar', '-f', 'yoda=/bar/baz', '-e', 'nono']
        files = {}
        with patch('builtins.open', mock_open(read_data='foo=bar\nbar=baz')):
            _add_variables(args, files)
        self.assertEqual(len(files), 1)
        self.assertIn('variables', files)
        self.assertIn('yada=foo/bar', files['variables'])
        self.assertIn('foo=bar', files['variables'])
        self.assertIn('bar=baz', files['variables'])

    def test_add_variables_readfromfile_invalid(self):
        args = ['foo=bar', '-e', 'yada=foo/bar', '-f', 'yoda=/bar/baz', '-e', 'nono']
        files = {}
        with patch('builtins.open', mock_open(read_data='foo=bar\nbar<-baz')):
            self.assertRaises(SystemExit, _add_variables, args, files)

    # kill_workflow

    def test_kill_workflow_bad_all_selectors(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', 'kill', 'workflow', '--all', '-l=foo==bar']
        ):
            self.assertRaises(SystemExit, delete_workflow, [])
        mock_get.assert_not_called()
        mock_delete.assert_not_called()

    def test_kill_workflow_bad_nothing(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', 'kill', 'workflow']
        ):
            self.assertRaises(SystemExit, delete_workflow, [])
        mock_get.assert_not_called()
        mock_delete.assert_not_called()

    def test_kill_workflow_all_nothing(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        mock_gw = MagicMock(return_value=[])
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', 'kill', 'workflow', '--all']
        ), patch(
            'opentf.tools.ctlworkflows._get_workflows', mock_gw
        ):
            self.assertRaises(SystemExit, delete_workflow, [])
        mock_get.assert_not_called()
        mock_delete.assert_not_called()
        mock_gw.assert_called_once()

    def test_kill_workflow_all_dryrun_reason(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        mock_gw = MagicMock(return_value=[])
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv',
            ['', 'kill', 'workflow', '--all', '--dry-run', '--reason', 'WHY'],
        ), patch(
            'opentf.tools.ctlworkflows._get_workflows', mock_gw
        ):
            self.assertRaises(SystemExit, delete_workflow, [])
        mock_get.assert_not_called()
        mock_delete.assert_not_called()
        mock_gw.assert_called_once()

    def test_kill_workflow_bad_uuid_all(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', 'kill', 'workflow', UUID_OK, '--all']
        ):
            self.assertRaises(SystemExit, delete_workflow, [UUID_OK])
        mock_get.assert_not_called()
        mock_delete.assert_not_called()

    def test_kill_workflow_200(self):
        mockresponse = _make_response(200, None)
        mock_get = MagicMock(return_value=mockresponse)
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            delete_workflow([UUID_OK])
        mock_get.assert_called_once()
        mock_delete.assert_called_once()

    def test_kill_workflow_404(self):
        mock_get = MagicMock(return_value=_make_response(404, None))
        mock_delete = MagicMock(return_value=_make_response(200, None))
        with patch('requests.delete', mock_delete), patch(
            'requests.get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            self.assertRaises(SystemExit, delete_workflow, [UUID_OK])
        mock_get.assert_called_once()
        mock_delete.assert_not_called()

    def test_kill_workflow_baduuid(self):
        self.assertRaises(SystemExit, delete_workflow, [UUID_BAD])

    # _get_first_page

    def test_get_first_page_404(self):
        mockresponse = Response()
        mockresponse.status_code = 404
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get_first_page, 'a1b2c3')
        mock_get.assert_called_once()

    def test_get_first_page_500(self):
        mockresponse = Response()
        mockresponse.status_code = 500
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get_first_page, 'a1b2c3')
        mock_get.assert_called_once()

    def test_get_first_page_200(self):
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = _get_first_page('a1b2c3')
        mock_get.assert_called_once()
        self.assertIsInstance(result, Response)

    # _filter_workflows

    def test_filter_workflows_nofilter(self):
        args = ['', 'get', 'workflows']
        mock_wfs = [
            {'metadata': {'workflow_id': 'a1'}},
            {'metadata': {'workflow_id': 'b1'}},
        ]
        with patch('sys.argv', args), patch(
            'opentf.tools.ctlworkflows._get_first_page'
        ) as mock_gfp:
            result = _filter_workflows(mock_wfs)
        self.assertEqual(len(result), len(mock_wfs))
        mock_gfp.and_assert_not_called()

    def test_filter_workflows_none(self):
        args = ['', 'get', 'workflows', '--having', 'foobar']
        mock_wfs = [
            {'metadata': {'workflow_id': 'a1'}},
            {'metadata': {'workflow_id': 'b1'}},
        ]
        mock_page = MagicMock()
        mock_page.json = lambda: {'details': {'items': []}}
        mock_gfp = MagicMock(return_value=mock_page)
        with patch('sys.argv', args), patch(
            'opentf.tools.ctlworkflows._get_first_page', mock_gfp
        ):
            result = _filter_workflows(mock_wfs)
        self.assertFalse(result)

    # list_workflows

    def test_list_workflows_ok_noworkflows(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(return_value=OBSERVER_WORKFLOWS_EMPTY)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            list_workflows()
        mock_get.assert_called_once()

    def test_list_workflows_badconf_couldnotconnect(self):
        mock_couldnotconnect = MagicMock(side_effect=SystemExit())
        mock_get = MagicMock(side_effect=exceptions.ConnectionError)
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlnetworking._could_not_connect', mock_couldnotconnect
        ), patch(
            'requests.get', mock_get
        ):
            self.assertRaises(SystemExit, list_workflows)
        mock_couldnotconnect.assert_called_once()

    def test_list_workflows_ok_worklows(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(return_value=OBSERVER_WORKFLOWS)
        mock_get = MagicMock(return_value=mockresponse)
        mock2response = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock2_getfirstpage = MagicMock(return_value=mock2response)
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get_first_page', mock2_getfirstpage
        ):
            list_workflows()
        mock_get.assert_called_once()
        self.assertEqual(mock2_getfirstpage.call_count, 2)

    def test_list_workflows_ok_worklows_dict(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(
            return_value={'details': {'items': {'a1': {}, 'a2': {}}}}
        )
        mock_get = MagicMock(return_value=mockresponse)
        mock2response = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock2_getfirstpage = MagicMock(return_value=mock2response)
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get_first_page', mock2_getfirstpage
        ):
            list_workflows()
        mock_get.assert_called_once()
        self.assertEqual(mock2_getfirstpage.call_count, 0)

    # run_workflow

    def test_run_workflow_201(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock()
        with patch('requests.post', mock_post), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlworkflows._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )
        mock_get.assert_not_called()

    def test_run_workflow_201_wait(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock(return_value=_make_response(200, {}))
        mock_getworkflow = MagicMock()
        with patch('requests.post', mock_post), patch('requests.get', mock_get), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', '--wait']
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow', mock_getworkflow
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )
        mock_getworkflow.assert_called_once()
        mock_get.assert_called_once()

    def test_run_workflow_wait_exit(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock(return_value=_make_response(200, {}))
        mock_getworkflow = MagicMock(side_effect=Exception('ooOps'))
        with patch('requests.post', mock_post), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlworkflows._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', '--wait']
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow', mock_getworkflow
        ):
            self.assertRaises(SystemExit, run_workflow, 'fooBar')
        mock_post.assert_called_once()
        mock_get.assert_called_once()
        mock_getworkflow.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )

    def test_run_workflow_201_legacy(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock()
        with patch('requests.post', mock_post), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlworkflows._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        mock_get.assert_not_called()
        self.assertEqual(mock_post.call_args[0][0], 'http://aaaa:4321/workflows')

    def test_run_workflow_401(self):
        args = ['', 'run', 'workflow', '_']
        mockresponse = _make_response(
            401,
            {
                'message': 'yada',
                'details': {'error': 'woRKflow'},
            },
        )
        mock_post = MagicMock(return_value=mockresponse)
        mock_handle = MagicMock(side_effect=SystemExit)
        with patch('sys.argv', args), patch('requests.post', mock_post), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlworkflows._handle_maybe_details', mock_handle
        ):
            self.assertRaises(SystemExit, run_workflow, 'fooBar')
        mock_post.assert_called_once()
        mock_handle.assert_called_once()

    def test_run_workflow_with_selectors(self):
        args = ['', '-w', '--field-selector', 'one=tWo', '-l', 'tHree=FouR']
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('requests.post', mock_post), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlworkflows._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', args
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow'
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        mock_get.assert_called_once()
        self.assertIn(
            'status?per_page=1',
            mock_get.call_args[0][0],
        )
        self.assertEqual('one=tWo', mock_get.call_args[1]['params']['fieldSelector'])
        self.assertEqual('tHree=FouR', mock_get.call_args[1]['params']['labelSelector'])

    def test_run_workflow_201_watch_and_apply_qualitygate(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock(return_value=_make_response(200, {}))
        mock_getworkflow = MagicMock()
        mock_qualitygate = MagicMock()
        with patch('requests.post', mock_post), patch('requests.get', mock_get), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', '--wait', '--mode=strict']
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow', mock_getworkflow
        ), patch(
            'opentf.tools.ctlworkflows._apply_qualitygate', mock_qualitygate
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )
        mock_getworkflow.assert_called_once()
        mock_get.assert_called_once()
        mock_qualitygate.assert_called_once_with('woRKflow_Id', True)

    def test_run_workflow_201_do_not_watch_apply_qualitygate(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'woRKflow_Id'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock(return_value=_make_response(200, {}))
        mock_getworkflow = MagicMock()
        mock_qualitygate = MagicMock()
        with patch('requests.post', mock_post), patch('requests.get', mock_get), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', '--mode=strict']
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow', mock_getworkflow
        ), patch(
            'opentf.tools.ctlworkflows._apply_qualitygate', mock_qualitygate
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )
        mock_getworkflow.assert_not_called()
        mock_get.assert_called_once()
        mock_qualitygate.assert_called_once_with('woRKflow_Id', False)

    def test_run_workflow_201_download_reports(self):
        mockresponse = _make_response(201, {'details': {'workflow_id': 'wFf'}})
        mock_post = MagicMock(return_value=mockresponse)
        mock_get = MagicMock(return_value=_make_response(200, {}))
        mock_getworkflow = MagicMock()
        mock_download = MagicMock()
        with patch('requests.post', mock_post), patch('requests.get', mock_get), patch(
            'builtins.open', mock_open(read_data='{}')
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'sys.argv', ['', '--report=executionlog.txt']
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows.get_workflow', mock_getworkflow
        ), patch(
            'opentf.tools.ctlworkflows._download_execution_reports', mock_download
        ):
            run_workflow('fooBar')
        mock_post.assert_called_once()
        self.assertEqual(
            mock_post.call_args[0][0], 'http://aaaa:4321/reCeptioNist/workflows'
        )
        mock_getworkflow.assert_not_called()
        mock_get.assert_called_once()
        mock_download.assert_called_once_with('wFf', False, ['executionlog.txt'])

    # _apply_qualitygate

    def test_apply_qualitygate_workflow_done(self):
        mock_get_page = MagicMock()
        mock_qualitygate = MagicMock()
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_get_page), patch(
            'opentf.tools.ctlworkflows.get_qualitygate', mock_qualitygate
        ), patch('opentf.tools.ctlworkflows._get_arg', MagicMock()):
            _apply_qualitygate('wFId', True)
        mock_get_page.assert_not_called()
        mock_qualitygate.assert_called_once()
        call = mock_qualitygate.call_args
        self.assertEqual('wFId', call[0][0])
        self.assertFalse(call[0][2])

    def test_apply_qualitygate_workflow_running_workflow_done(self):
        mock_response_running = _make_response(201, {'details': {'status': 'RUNNING'}})
        mock_response_done = _make_response(201, {'details': {'status': 'DONE'}})
        mock_get_page = MagicMock(
            side_effect=[mock_response_running, mock_response_done]
        )
        mock_qualitygate = MagicMock()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_get_page), patch(
            'opentf.tools.ctlworkflows.get_qualitygate', mock_qualitygate
        ), patch('opentf.tools.ctlworkflows._get_arg', MagicMock()), patch(
            'builtins.print', mock_print
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            _apply_qualitygate('wFId', False)
        self.assertEqual(2, mock_get_page.call_count)
        mock_qualitygate.assert_called_once()
        call = mock_qualitygate.call_args
        self.assertEqual('wFId', call[0][0])
        self.assertFalse(call[0][2])

    def test_apply_qualitygate_workflow_running_workflow_failed(self):
        mock_response_running = _make_response(201, {'details': {'status': 'RUNNING'}})
        mock_response_failed = _make_response(201, {'details': {'status': 'FAILED'}})
        mock_get_page = MagicMock(
            side_effect=[mock_response_running, mock_response_failed]
        )
        mock_qualitygate = MagicMock()
        mock_error = MagicMock()
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_get_page), patch(
            'opentf.tools.ctlworkflows.get_qualitygate', mock_qualitygate
        ), patch('opentf.tools.ctlworkflows._get_arg', MagicMock()), patch(
            'opentf.tools.ctlworkflows._error', mock_error
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            self.assertRaisesRegex(SystemExit, '1', _apply_qualitygate, 'wFId', False)
            mock_qualitygate.assert_not_called()
            mock_error.assert_called_once_with(
                'Workflow %s failed, quality gate not applied.', 'wFId'
            )

    # _download_execution_reports

    def test_download_execution_reports_ok_running_done(self):
        mock_response_running = _make_response(201, {'details': {'status': 'RUNNING'}})
        mock_response_done = _make_response(201, {'details': {'status': 'DONE'}})
        mock_get_page = MagicMock(
            side_effect=[mock_response_running, mock_response_done]
        )
        mock_download = MagicMock()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_get_page), patch(
            'opentf.tools.ctlattachments.download_workflow_reports', mock_download
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows._warning'
        ) as mock_warning:
            _download_execution_reports(
                'wFf',
                False,
                [
                    'executionlog.txt:foo.txt',
                    'executionreport.html:a/bar.html',
                    'executionreport.html:b/foo.html',
                    'fdsklfsjdksl',
                ],
            )
        mock_download.assert_called_once()
        self.assertEqual(
            {
                'executionlog.txt': {'foo.txt'},
                'executionreport.html': {'a/bar.html', 'b/foo.html'},
            },
            mock_download.call_args[0][1],
        )
        mock_warning.assert_called_once()
        self.assertIn('Unexpected report name', mock_warning.call_args[0][0])

    # get_workflow

    def test_get_workflow_ok(self):
        args = ['', 'get', 'workflow', '_']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_keyboardinterrupt(self):
        args = ['', 'get', 'workflow', '_']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows.emit_event',
            MagicMock(side_effect=KeyboardInterrupt),
        ):
            self.assertRaises(SystemExit, get_workflow, UUID_OK)
        self.assertEqual(mock_get.call_count, 1)

    def test_get_workflow_brokenpipe(self):
        args = ['', 'get', 'workflow', '_']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows.emit_event',
            MagicMock(side_effect=BrokenPipeError),
        ):
            self.assertRaises(SystemExit, get_workflow, UUID_OK)
        mock_get.assert_called_once()

    def test_get_workflow_ok_workflownotfirstevent(self):
        args = ['', 'get', 'workflow', '_']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_SPURIOUS_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_ok_failedworkflow(self):
        args = ['', 'get', 'workflow', '_']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_json(self):
        args = ['', '-o', 'json']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)
        mock_print.assert_called()

    def test_get_workflow_notifs(self):
        args = ['', '--show-notifications']
        mockresponse = _make_response(
            200, OBSERVER_FIRST_PAGE_NOTIFICATIONS_SPURIOUS_OK
        )
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_notifs_short(self):
        args = ['', '-a']
        mockresponse = _make_response(
            200, OBSERVER_FIRST_PAGE_NOTIFICATIONS_SPURIOUS_OK
        )
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_outputyaml(self):
        args = ['', '--output=yaml']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertIsNone(get_workflow(UUID_OK))
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_with_selectors(self):
        args = ['', '--field-selector', 'one=tWo', '-l', 'tHree=FouR']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_DONE_OK)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        self.assertEqual('one=tWo', mock_get.call_args[1]['params']['fieldSelector'])
        self.assertEqual('tHree=FouR', mock_get.call_args[1]['params']['labelSelector'])

    def test_get_workflow_bad_step_depth(self):
        args = ['', '--step-depth=yada']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertRaises(SystemExit, get_workflow, UUID_OK)
        mock_get.assert_not_called()

    def test_get_workflow_bad_job_depth(self):
        args = ['', '--job-depth', 'yada']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertRaises(SystemExit, get_workflow, UUID_OK)
        mock_get.assert_not_called()

    def test_get_workflow_bad_max_command_length(self):
        args = ['', '--max-command-length', '123aaab']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_FAILED)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch.object(
            sys, 'argv', args
        ), patch(
            'builtins.print', mock_print
        ):
            self.assertRaises(SystemExit, get_workflow, UUID_OK)
        mock_get.assert_not_called()

    def test_get_workflow_missing_creationtimestamp(self):
        args = ['', 'get', 'workflow', 'xxx']
        mockresponse = _make_response(200, OBSERVER_FIRST_PAGE_SPURIOUS_NOCTS_OK)
        mock_get = MagicMock(return_value=mockresponse)
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)

    def test_get_workflow_show_attachments(self):
        args = ['', 'get', 'workflow', 'xxx', '--show-attachments']
        mockresponse = _make_response(200, OBSERVER_ATTACHMENTS_OK)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        calls = mock_print.mock_calls
        self.assertEqual('Workflow completed successfully.', calls[-1][1][0])
        self.assertIn(
            'Produced attachment af0edfc3-6cc6-4a25-912b-e11d1d09b752 (executionlog.txt)',
            calls[-2][1][0],
        )
        self.assertIn(
            'Produced attachment 5b444910-a246-4d66-b02b-40e77148b90d (output.xml).',
            calls[-5][1][0],
        )

    def test_get_workflow_show_attachments_useful_only(self):
        args = ['', 'get', 'workflow', 'xxx', '--show-attachments']
        mockresponse = _make_response(200, OBSERVER_NOT_USEFUL_ATTACHMENTS)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        calls = mock_print.mock_calls
        self.assertEqual(13, mock_print.call_count)
        self.assertEqual('Workflow completed successfully.', calls[-1][1][0])
        self.assertIn(
            'Produced attachment b10a8635-0d36-4f40-8776-d9ac741e1b0f (allure-report.tar).',
            calls[-2][1][0],
        )

    def test_get_workflow_show_attachments_only_useful_only(self):
        args = ['', 'get', 'workflow', 'xxx', '--show-attachments-only']
        mockresponse = _make_response(200, OBSERVER_NOT_USEFUL_ATTACHMENTS)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        calls = mock_print.mock_calls
        self.assertEqual(5, mock_print.call_count)
        self.assertEqual('Workflow completed successfully.', calls[-1][1][0])
        self.assertIn(
            'Produced attachment b10a8635-0d36-4f40-8776-d9ac741e1b0f (allure-report.tar).',
            calls[-2][1][0],
        )

    def test_get_workflow_show_attachments_only(self):
        args = ['', 'get', 'workflow', 'xxx', '--show-attachments-only']
        mockresponse = _make_response(200, OBSERVER_ATTACHMENTS_OK)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        calls = mock_print.mock_calls
        self.assertEqual('Workflow completed successfully.', calls[-1][1][0])
        self.assertIn(
            'Produced attachment af0edfc3-6cc6-4a25-912b-e11d1d09b752 (executionlog.txt)',
            calls[-2][1][0],
        )
        self.assertIn(
            'Produced attachment 5b444910-a246-4d66-b02b-40e77148b90d (output.xml).',
            calls[-5][1][0],
        )
        self.assertIn('for test reference `some test case`', calls[5][1][3])

    def test_get_workflow_show_attachments_only_allure(self):
        args = ['', 'get', 'workflow', 'xxx', '--show-attachments-only']
        mockresponse = _make_response(200, OBSERVER_ALLURE)
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('sys.argv', args), patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'builtins.print', mock_print
        ):
            get_workflow(UUID_OK)
        self.assertEqual(mock_get.call_count, 2)
        calls = mock_print.mock_calls
        self.assertEqual('Workflow completed successfully.', calls[-1][1][0])
        self.assertIn(
            'Produced attachment b10a8635-0d36-4f40-8776-d9ac741e1b0f (allure-report.tar)',
            calls[-2][1][0],
        )

    # _get_outputformat

    def test_get_outputformat_unexpectedxml(self):
        args = ['', '-o', 'xml']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_outputformat, allowed=('json', 'yaml'))

    def test_get_outputformat_incompleteoption(self):
        args = ['', '-a', '-o']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_outputformat, allowed=('json', 'yaml'))

    def test_get_outputformat_ok_json_short(self):
        args = ['', '-o', 'json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    def test_get_outputformat_ok_json_long(self):
        args = ['', '--output=json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    def test_get_outputformat_ok_json_longlong(self):
        args = ['', '--output', 'json', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_outputformat(allowed=('json', 'yaml'))
        self.assertEqual(result, 'json')

    def test_get_outputformat_badorder(self):
        args = ['', 'json', 'foo', '-o']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, _get_outputformat, allowed=('json', 'yaml'))

    # handlers

    def test_handler_maybe_details_500_ithout_details(self):
        mockresponse = _make_response(500, {'message': 'meSSagE'})
        mock_error = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error):
            self.assertRaises(SystemExit, _handle_maybe_details, mockresponse)
        mock_error.assert_called_once()

    def test_handler_maybe_details_500_with_details(self):
        mockresponse = _make_response(
            500,
            {'message': 'meSSagE', 'details': {'error': 'eRRoR'}},
        )
        mock_error = MagicMock()
        with patch('opentf.tools.ctlworkflows._error', mock_error):
            self.assertRaises(SystemExit, _handle_maybe_details, mockresponse)
        self.assertEqual(mock_error.call_count, 2)

    # _get_workflow_events

    def test_get_workflow_events_ok_newconfig(self):
        mock_gfp_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_gfp_response.headers['Link'] = '<http://example.com/foo>; rel="next"'
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = lambda: OBSERVER_SECOND_PAGE_OK  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG
        ), patch('opentf.tools.ctlworkflows.CONFIG', CONFIG), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            what = list(_get_workflow_events(UUID_OK, False))
        mock_gfp.assert_called_once()
        mock_get.assert_called_once()
        self.assertTrue(
            mock_get.call_args[0][0].startswith('http://aaaa:9999/workflows/')
        )
        self.assertEqual(len(what), 2)

    def test_get_workflow_events_ok_legacyconfig(self):
        mock_gfp_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_gfp_response.headers['Link'] = '<http://example.com/foo>; rel="next"'
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = lambda: OBSERVER_SECOND_PAGE_OK  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlworkflows.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            what = list(_get_workflow_events(UUID_OK, False))
        mock_gfp.assert_called_once()
        mock_get.assert_called_once()
        self.assertEqual(mock_get.call_args[0][0], 'http://example.com/foo')
        self.assertEqual(len(what), 2)

    def test_get_workflow_events_ok_done(self):
        mock_gfp_response = _make_response(200, OBSERVER_FIRST_PAGE_RUNNING_OK)
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = lambda: OBSERVER_FIRST_PAGE_DONE_OK  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ):
            what = list(_get_workflow_events(UUID_OK, True))
        mock_gfp.assert_called_once()
        mock_get.assert_called_once()
        self.assertEqual(len(what), 2)

    def test_get_workflow_events_selectors_KO(self):
        mock_gfp_response = _make_response(200, OBSERVER_WORKFLOWS_EMPTY_RUNNING)
        mock_gfp = MagicMock(return_value=mock_gfp_response)
        mock_get_response = Response()
        mock_get_response.status_code = 200
        mock_get_response.json = (
            lambda: OBSERVER_WORKFLOWS_EMPTY_DONE
        )  # pyright: ignore
        mock_get = MagicMock(return_value=mock_get_response)
        mock_warning = MagicMock()
        args = ['', '--field-selector', 'kind=foo']
        with patch('opentf.tools.ctlworkflows._get_first_page', mock_gfp), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlworkflows._get', mock_get
        ), patch.object(
            sys, 'argv', args
        ), patch(
            'opentf.tools.ctlworkflows.sleep'
        ), patch(
            'opentf.tools.ctlworkflows._warning', mock_warning
        ):
            what = list(_get_workflow_events(UUID_OK, True))
        mock_warning.assert_called_once_with(
            "Could not find items matching selectors: {'fieldSelector': 'kind=foo'}"
        )
        mock_gfp.assert_called_once()
        mock_get.assert_called_once()
        self.assertEqual('kind=foo', mock_get.call_args[1]['params']['fieldSelector'])
        self.assertEqual(len(what), 0)

    # _emit_command

    def test_emit_command_empty(self):
        mock_print = MagicMock()
        mock_context = MagicMock()
        with patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlworkflows._emit_prefix', MagicMock()
        ):
            _emit_command(
                {'metadata': {'step_sequence_id': 0}, 'scripts': []},
                mock_context,
                False,
            )
        self.assertEqual(mock_print.call_count, 2)
        self.assertEqual(mock_print.call_args[0][1], 'None')

    def test_emit_command_short(self):
        mock_print = MagicMock()
        mock_context = MagicMock()
        mock_context.max_command_length = 10
        with patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlworkflows._emit_prefix', MagicMock()
        ):
            _emit_command(
                {'metadata': {'step_sequence_id': 0}, 'scripts': ['short']},
                mock_context,
                False,
            )
        self.assertEqual(mock_print.call_count, 2)
        self.assertEqual(mock_print.call_args[0][1], 'short')

    def test_emit_command_truncated(self):
        mock_print = MagicMock()
        mock_context = MagicMock()
        mock_context.max_command_length = 15
        with patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlworkflows._emit_prefix', MagicMock()
        ):
            _emit_command(
                {
                    'metadata': {'step_sequence_id': 0},
                    'scripts': ['a very long command'],
                },
                mock_context,
                False,
            )
        self.assertEqual(mock_print.call_count, 2)
        self.assertEqual(mock_print.call_args[0][1], 'a very long com...')
