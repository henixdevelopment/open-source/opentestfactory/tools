# Copyright (c) 2022-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import json
import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch, mock_open

import cryptography
import jwt

from requests import Response, exceptions

from opentf.tools.ctlnetworking import (
    _could_not_connect,
    _get,
    _get_json,
    _get_file,
    _head,
    _delete,
    _post,
    _make_hostport,
    _get_workflows,
    _handle_maybe_outdated,
)

import opentf.tools.ctl


########################################################################
# Datasets

CONFIG = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'services': {
            'eventbus': {'port': 1234},
            'receptionist': {'port': 4321, 'prefix': 'reCeptioNist'},
            'observer': {'port': 9999, 'force-base-url': True},
            'killswitch': {'port': 8888},
        },
        'server': 'http://aaaa',
    },
}

CONFIG_NOSERVER = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'services': {
            'eventbus': {'port': 1234},
            'receptionist': {'port': 4321, 'prefix': 'reCeptioNist'},
            'observer': {'port': 9999, 'force-base-url': True},
            'killswitch': {'port': 8888},
        },
        'no_server': 'http://aaaa',
    },
}

CONFIG_LEGACY = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'max-retry': 1,
        'polling-delay': 1,
        'warmup-delay': 1,
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'http://aaaa',
    },
}

CONFIG_NEW_LEGACY = {
    'token': 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333',
    'orchestrator': {
        'insecure-skip-tls-verify': True,
        'services': {
            'eventbus': {'port': 1234},
            'receptionist': {'port': 4321, 'prefix': 'reCeptioNist'},
            'observer': {'port': 9999, 'force-base-url': True},
            'killswitch': {'port': 8888},
        },
        'ports': {
            'eventbus': 1234,
            'receptionist': 4321,
            'observer': 9999,
            'killswitch': 8888,
        },
        'server': 'http://aaaa',
    },
}


HEADERS = {'Authorization': 'Bearer ' + CONFIG_LEGACY['token']}

SUBSCRIPTION_MANIFEST = {
    'metadata': {
        'name': 'nAMe',
        'creationTimestamp': 'tIMeSTAMp',
        'annotations': {},
    },
    'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
    'status': {'publicationCount': 123},
}

AGENTREGISTRATION_MANIFEST_BUSY = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'AgentRegistration',
    'metadata': {
        'name': 'aGENtREgistration',
        'creationTimestamp': 'crEAtioNTimestamp',
        'agent_id': 'aGenT_Id',
        'namespaces': 'a,b',
    },
    'spec': {
        'encoding': 'utf-8',
        'script_path': 'scRIPt_pAtH',
        'tags': ['windows', 'roboTFRamework'],
    },
    'status': {
        'communicationCount': 12,
        'communicationStatusSummary': {},
        'lastCommunicationTimestamp': 'laSTCOMmunicationTimestamp',
        'currentJobID': 'a12B34c56',
    },
}

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

CHANNEL_MANIFEST_IDLE = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': 'bar',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'IDLE',
        'currentJobID': None,
    },
}

CHANNEL_MANIFEST_BUSY = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': 'bar,foo',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'BUSY',
        'currentJobID': UUID_OK,
    },
}

CHANNEL_MANIFEST_PENDING = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'Channel',
    'metadata': {
        'name': 'foo',
        'namespaces': '*',
        'channelhandler_id': 'uuid',
    },
    'spec': {'tags': ['a', 'b', 'c']},
    'status': {
        'lastCommunicationTimestamp': 'atimestamp',
        'phase': 'PENDING',
        'currentJobID': UUID_OK,
    },
}

TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJleGFtcGxlLmNvbSIsInN1YiI6Im5vYm9keSJ9.ABsPbmeakwzxS-brB8BZcqMMonqolTCG6PJwcxACSMkl7lLURocEXzybLxhhlarQuCFt3wpZpEeDRoXjlLketl-ZruzQQ889730KD1m1VvBBzwpulrrv9uWLY9TVjnC7r-j3Rhf758HDCpozFy-Q6fFP1mgDL_ZqVZVFOUWkQeI-dzwebzLfsz7Y_KZsuwTvimpjzQ8GePMwjp0sPVmEAuC8GDPiUFA2WbHJGrS1LQGSD5T9j8QsXnv_uaVpag5KFzfN0ZG0jFZe4UBiPi7Grq_WjAc8lA9GrAduV5m7SP8jl4DMA0ufeRAFLzdlg53qRPp8o2Lr1IjX_qBxp6ZrpIS_bjbkI41WEaHNreFK6Zh2eqz-Ms60RphYQw_hDSqlwc402WLGvwCururKCzDelKAXSS0HFlWPS4cY7wHDJmRy76XOYNrsxoAAg__Ci1FspzWw8ngNvkMnwuQpRrYsOhQ4W0orNTpYtQWXdt8HKm2fb-lXe5Rmxev9lZnqKe77HNET8ITKPz7pC_ZVCcV4GneMY3ryzUAMWMM2bBzTkjvWSCMxSCMa7vptL-nCSZro8H1GYzWY9PAW0ZU5Mj7-iYO1A0ciHnDkJziQNRTksD30qt45se1ot8G4Kt8P6vxxsi-mgAjYnZGOH_g5EW54q806cMq4vJ-DLn-Llov3Y3s'

TOKEN_WITH_EXP = 'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJoZW5peCIsInN1YiI6Im1sZiIsImV4cCI6MTY3MDEwODQwMH0.znUyjEuHJmITqmajzDqWeTU3p2DyVIJyFnxArHkr8EUv90QzaTahw7X6Dy9VknFB-dvRGv41uO0jcuOmJETomNnNU9wPq2slELimyoqKRHpKMWgfT2dD2yyUI1d0kGhHsYelM4YMx71OJQbv7dr5BpMA7iwr6MEbGv0NIM4BOh5wpzTrNfEJgBCZp62K5UKUbtJVsFDtevSzujYTentcl3YyYryLwHOkRCGWEH2g3S82-OqeUY4eCvCri8FCn6a9VEZajXYYjEUWcxRaNKOH_di8XNCsTI0rAIVWFV1TCBgkOuDn4krNSPGyU54WImbbFiGcuTV_G0sV8z9Vfr1lSj4ddcktpaq6B3n4cx_AA2z7y2TCruUgT4LNUZRn-zZjuL693X2XKdlZqhCgvlKK1v1MNQQtK6Avf3v1q_uL-6enqVSKSQvHyfkjZF2NAOlBUpbiNY2EhuXY94pHqjkdpnAkM7S-uPlaPZXeIhV3l7KRtoKSig4LUEdRz3HnJA0cvalvxu5Ld8w5N0brqmTG71Pp60gBAUi-lW7cb1e35zwgSZNg6SQ0inbr8Dgk50nVJZrxcGH37zI68AQ62tIq5yPO25D0OzvnoYT4ZQ7mZJcctmRuT8oauzmyV-0vmxgR8h6_hBTBFxj5K8reyWPQlgOs-3bARnLkyKGBUOJycAU'

BOM = {
    'details': {
        'items': {
            "base-images": [
                {
                    "name": "allinone",
                    "version": "nightly",
                }
            ],
            "internal-components": [
                {
                    "name": "squash-freemium",
                    "version": "0.6.0.dev70+main.95e8b0a1",
                },
            ],
            "name": "squash-orchestrator",
        }
    }
}

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


def _generate_token():
    """Generate temporary key and JWT token."""
    # pylint: disable=import-outside-toplevel
    from cryptography.hazmat.primitives import serialization
    from cryptography.hazmat.primitives.asymmetric import rsa
    from cryptography.hazmat.backends import default_backend

    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=4096, backend=default_backend()
    )
    public_key = private_key.public_key()
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    ).decode()
    pub = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    token = jwt.encode(
        {'iss': 'check_token_unittest', 'sub': 'temp token'},
        pem,
        algorithm='RS512',
    )
    return token, str(pub, encoding='utf-8')


class TestCtl(unittest.TestCase):
    """Unit tests for ctlnetworking & ctl."""

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # ctlnetworking

    # _could_not_connect

    def test_could_not_connect_proxyerror(self):
        self.assertRaises(
            SystemExit, _could_not_connect, 'foo', exceptions.ProxyError('oops')
        )

    def test_could_not_connect_sslerror(self):
        self.assertRaises(
            SystemExit, _could_not_connect, 'foo', exceptions.SSLError('oops')
        )

    # _get

    def test_get_ok_200(self):
        mockresponse = _make_response(200, {})
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _get('foo')
        self.assertEqual(response, {})

    def test_get_404_nohandler(self):
        mockresponse = _make_response(404, {})
        mock_get = MagicMock(return_value=mockresponse)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _get, 'foo')

    def test_get_404_handler(self):
        mockresponse = _make_response(404, {})
        mock_get = MagicMock(return_value=mockresponse)
        mock_handler = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _get('foo', handler=mock_handler)
        self.assertEqual(mock_handler.call_count, 1)

    # _get_json

    def test_get_json_notdict(self):
        mock_get = MagicMock(return_value='notAdiCt')
        with patch('opentf.tools.ctlnetworking._get', mock_get):
            self.assertRaises(SystemExit, _get_json, 'aPatH')

    # _get_file

    def test_get_file_ok_200(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.content = 'abcdef'
        mock_get = MagicMock(return_value=mock_response)
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _get_file('foo')
        self.assertEqual(response.content, 'abcdef')
        mock_get.assert_called_once()
        self.assertTrue(mock_get.call_args[1]['stream'])
        self.assertEqual('foo', mock_get.call_args[0][0])

    def test_get_file_404(self):
        mock_response = MagicMock()
        mock_response.status_code = 404
        mock_response.text = 'Error 404'
        mock_get = MagicMock(return_value=mock_response)
        mock_error = MagicMock()
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlnetworking._error', mock_error
        ):
            self.assertRaises(SystemExit, _get_file, 'foo')
        mock_error.assert_called_once_with(
            'Could not query foo, got %d: %s.', 404, 'Error 404'
        )

    def test_get_file_ko_exception(self):
        mock_get = MagicMock(side_effect=Exception('Oops'))
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlnetworking._fatal', mock_fatal
        ):
            self.assertRaises(SystemExit, _get_file, 'foo')
        mock_fatal.assert_called_once()
        self.assertEqual('Could not query foo: %s.', mock_fatal.call_args[0][0])

    def test_get_file_ko_connectionerror(self):
        mock_get = MagicMock(side_effect=exceptions.ConnectionError('No connection'))
        mock_cnc = MagicMock(side_effect=Exception('Oops'))
        with patch('requests.get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlnetworking._could_not_connect', mock_cnc
        ):
            self.assertRaises(Exception, _get_file, 'foo')
        mock_cnc.assert_called_once()
        self.assertTrue('No connection', str(mock_cnc.call_args[0][1]))

    # _head

    def test_head_ok_200(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {'a': 'b'}
        mock_head = MagicMock(return_value=mock_response)
        with patch('requests.head', mock_head), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            _head('foo')
        mock_head.assert_called_once()

    def test_head_ko_500(self):
        mock_response = MagicMock()
        mock_response.status_code = 500
        mock_response.text = 'moo'
        mock_head = MagicMock(return_value=mock_response)
        mock_error = MagicMock()
        with patch('requests.head', mock_head), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlnetworking._error', mock_error
        ):
            self.assertRaisesRegex(SystemExit, '1', _head, 'foo')
        mock_head.assert_called_once()
        mock_error.assert_called_once_with(
            'Could not obtain headers from foo, got %d: %s.', 500, 'moo'
        )

    def test_head_ko_exception(self):
        mock_head = MagicMock(side_effect=Exception('Booooo!'))
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('requests.head', mock_head), patch(
            'opentf.tools.ctlnetworking._fatal', mock_fatal
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            self.assertRaisesRegex(SystemExit, '2', _head, 'foo')
        mock_head.assert_called_once()
        mock_fatal.assert_called_once()
        self.assertEqual(
            'Could not obtain headers from foo: %s.', mock_fatal.call_args[0][0]
        )
        self.assertEqual('Booooo!', str(mock_fatal.call_args[0][1]))

    def test_head_ko_connectionerror(self):
        mock_head = MagicMock(
            side_effect=exceptions.ConnectionError('No no no connection')
        )
        mock_cnc = MagicMock(side_effect=Exception('Oops'))
        with patch('requests.head', mock_head), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlnetworking._could_not_connect', mock_cnc
        ):
            self.assertRaises(Exception, _head, 'foo')
        mock_cnc.assert_called_once()
        self.assertTrue('No no no connection', str(mock_cnc.call_args[0][1]))

    # _delete

    def test_delete_ok_200(self):
        mockresponse = _make_response(200, {})
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _delete('foo')
        self.assertEqual(response, {})
        self.assertEqual(mock_delete.call_count, 1)

    def test_delete_404_nohandler(self):
        mockresponse = _make_response(404, {})
        mock_delete = MagicMock(return_value=mockresponse)
        with patch('requests.delete', mock_delete), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _delete, 'foo')

    def test_delete_404_handler(self):
        mockresponse = _make_response(404, {})
        mock_delete = MagicMock(return_value=mockresponse)
        mock_handler = MagicMock()
        with patch('requests.delete', mock_delete), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _delete('foo', handler=mock_handler)
        self.assertEqual(mock_handler.call_count, 1)

    # _post

    def test_post_ok_200(self):
        mockresponse = _make_response(200, {})
        mock_post = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _post('foo')
        self.assertEqual(response, {})

    def test_post_404_nohandler(self):
        mockresponse = _make_response(404, {})
        mock_post = MagicMock(return_value=mockresponse)
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            self.assertRaises(SystemExit, _post, 'foo')

    def test_post_404_handler(self):
        mockresponse = _make_response(404, {})
        mock_post = MagicMock(return_value=mockresponse)
        mock_handler = MagicMock()
        with patch('requests.post', mock_post), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            response = _post('foo', handler=mock_handler)
        self.assertEqual(mock_handler.call_count, 1)

    # ctl

    # list_subscriptions

    def test_list_subscriptions(self):
        mockresponse = MagicMock()
        mockresponse.json = lambda: {'items': {'a1b2c3': SUBSCRIPTION_MANIFEST}}
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_subscriptions()
        self.assertIsNone(result)

    def test_list_subscriptions_bad_columns(self):
        mockresponse = {'items': {'a1b2c3': SUBSCRIPTION_MANIFEST}}
        mock_get = MagicMock(return_value=mockresponse)
        mock_gc = MagicMock(side_effect=ValueError('bAD_coLUMns'))
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._get_columns', mock_gc
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_subscriptions)

    # list_agents

    def test_list_agents_oldapiresponse(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(
            return_value={'items': {'a1b2c3': AGENTREGISTRATION_MANIFEST_BUSY}}
        )

        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_agents()
        self.assertIsNone(result)

    def test_list_agents_newapiresponse(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(
            return_value={'items': [AGENTREGISTRATION_MANIFEST_BUSY]}
        )
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_agents()
        self.assertIsNone(result)

    def test_list_agents_newapiresponse_unsupportedselectors(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(
            return_value={'items': [AGENTREGISTRATION_MANIFEST_BUSY]}
        )
        mockresponse.headers = {}
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.argv', ['', '--field-selector', '(windows) in spec.tags']
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_agents)

    def test_list_agents_newapiresponse_supportedselectors(self):
        mockresponse = MagicMock()
        mockresponse.json = MagicMock(
            return_value={'items': [AGENTREGISTRATION_MANIFEST_BUSY]}
        )
        mockresponse.headers = {'X-Processed-Query': 'fieldSelector'}
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'sys.argv', ['', '--field-selector', '(windows) in spec.tags']
        ):
            result = opentf.tools.ctl.list_agents()
        self.assertIsNone(result)

    def test_list_agents_bad_columns(self):
        mockresponse = {'items': [AGENTREGISTRATION_MANIFEST_BUSY]}
        mock_get = MagicMock(return_value=mockresponse)
        mock_gc = MagicMock(side_effect=ValueError('bAD_coLUMns'))
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._get_columns', mock_gc
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_agents)

    # list_channels

    def test_list_channels_ok(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_PENDING,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            result = opentf.tools.ctl.list_channels()
        self.assertIsNone(result)

    def test_list_channels_bad_columns(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_BUSY,
                    CHANNEL_MANIFEST_PENDING,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        mock_gc = MagicMock(side_effect=ValueError('bAD_coLUMns'))
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS), patch(
            'opentf.tools.ctlcommons._get_columns', mock_gc
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_channels)

    def test_list_channels_with_field_selector(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_IDLE,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        args = ['', '--field-selector', 'status.phase=IDLE']
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.list_channels()
        mock_get.assert_called_once()
        self.assertEqual(
            'status.phase=IDLE', mock_get.call_args[1]['params']['fieldSelector']
        )

    def test_list_channels_with_label_selector(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_IDLE,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        args = ['', '--selector', 'label=lAbEl']
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.list_channels()
        mock_get.assert_called_once()
        self.assertEqual(
            'label=lAbEl', mock_get.call_args[1]['params']['labelSelector']
        )

    def test_list_channels_with_label_and_field_selectors(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_IDLE,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        args = [
            '',
            '--selector',
            'label=lAbEl',
            '--field-selector',
            'metadata.name in (one, two, three)',
        ]
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._get', mock_get
        ), patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.list_channels()
        mock_get.assert_called_once()
        self.assertEqual(
            'metadata.name in (one, two, three)',
            mock_get.call_args[1]['params']['fieldSelector'],
        )
        self.assertEqual(
            'label=lAbEl', mock_get.call_args[1]['params']['labelSelector']
        )

    def test_list_channels_without_selectors(self):
        mockresponse = {
            'details': {
                'items': [
                    CHANNEL_MANIFEST_IDLE,
                ]
            }
        }
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch('opentf.tools.ctlnetworking.HEADERS', HEADERS):
            opentf.tools.ctl.list_channels()
        mock_get.assert_called_once()
        self.assertEqual('/channels', mock_get.call_args[0][1])

    # main

    def test_main_unknowncommand(self):
        args = ['', 'unknown']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)

    def test_main_help(self):
        args = ['', '--help']
        mock_printhelp = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.print_help', mock_printhelp
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock_printhelp.assert_called_once()

    def test_main_noparameter(self):
        args = ['']
        mock_print = MagicMock()
        with patch.object(sys, 'argv', args), patch('builtins.print', mock_print):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock_print.assert_called_once()

    def test_main_options(self):
        args = ['', 'options']
        mock_print = MagicMock()
        with patch.object(sys, 'argv', args), patch('builtins.print', mock_print):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock_print.assert_called_once()

    def test_main_version(self):
        args = ['', 'version']
        mock_gettoolsversion = MagicMock()
        mock_bom = {'details': {'items': {'name': 'fOO'}}}
        mock_get = MagicMock(return_value=mock_bom)
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.get_tools_version', mock_gettoolsversion
        ), patch('opentf.tools.ctl._get', mock_get):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock_gettoolsversion.assert_called_once()

    def test_main_version_debug(self):
        args = ['', 'version', '--debug']
        mock_getorchversion = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.get_orchestrator_version', mock_getorchversion
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl.get_tools_version'
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.main)
        mock_getorchversion.assert_called_once_with(True)

    def test_main_get_subscriptions(self):
        args = ['', 'get', 'subscriptions']
        mock_listsubs = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_subscriptions', mock_listsubs
        ), patch('opentf.tools.ctl.read_configuration'):
            opentf.tools.ctl.main()
        mock_listsubs.assert_called_once()

    def test_main_delete_subscription(self):
        sub_id = 'a1234-cafe'
        args = ['', 'delete', 'subscription', sub_id]
        mock_deletesub = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.delete_subscription', mock_deletesub
        ), patch('opentf.tools.ctl.read_configuration'):
            opentf.tools.ctl.main()
        mock_deletesub.assert_called_once()

    def test_main_get_channels(self):
        args = ['', 'get', 'channels']
        mock_listchans = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_channels', mock_listchans
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_listchans.assert_called_once()

    def test_main_get_agents(self):
        args = ['', 'get', 'agents']
        mock_listagents = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.list_agents', mock_listagents
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_listagents.assert_called_once()

    def test_main_delete_agent(self):
        agent_id = 'a1234-cafe'
        args = ['', 'delete', 'agent', agent_id]
        mock_deleteagent = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.delete_agent', mock_deleteagent
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_deleteagent.assert_called_once_with([agent_id])

    def test_main_config_view(self):
        args = ['', 'config', 'view']
        mock_configcmd = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.config_cmd', mock_configcmd
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_configcmd.assert_called_once()

    def test_main_get_workflows(self):
        args = ['', 'get', 'workflows']
        mock_workflowcmd = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.workflow_cmd', mock_workflowcmd
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_workflowcmd.assert_called_once()

    def test_main_get_workflow(self):
        args = ['', 'get', 'workflow']
        mock_workflowcmd = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.workflow_cmd', mock_workflowcmd
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_workflowcmd.assert_called_once()

    def test_main_get_qualitygate(self):
        args = ['', 'get', 'qualitygate']
        mock_qualitygatecmd = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.qualitygate_cmd', mock_qualitygatecmd
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_qualitygatecmd.assert_called_once()

    def test_main_cp_attachments(self):
        args = ['', 'cp']
        mock_attachments_cmd = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.attachments_cmd', mock_attachments_cmd
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_attachments_cmd.assert_called_once()

    def test_main_generate_report(self):
        args = ['', 'generate', 'report']
        mock_generate_report = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.generate_report_cmd', mock_generate_report
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_generate_report.assert_called_once()

    def test_main_get_datasources(self):
        args = ['', 'get', 'datasource', 'wf_id']
        mock_get_datasources = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.get_datasources_cmd', mock_get_datasources
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_get_datasources.assert_called_once()

    def test_main_completion_bash(self):
        args = ['', 'completion', 'bash']
        mock_completion = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl.completion_cmd', mock_completion
        ), patch('opentf.tools.ctl.read_configuration'), patch(
            'opentf.tools.ctl._get'
        ), patch(
            'opentf.tools.ctlnetworking.CONFIG', CONFIG_LEGACY
        ), patch(
            'opentf.tools.ctlnetworking.HEADERS', HEADERS
        ):
            opentf.tools.ctl.main()
        mock_completion.assert_called_once()

    # print_help

    def test_print_help_unknown_command(self):
        args = ['', 'unknown', '--help']
        self.assertRaises(SystemExit, opentf.tools.ctl.print_help, args)

    def test_print_help_delete_agent(self):
        args = ['', 'delete', 'agent']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_options(self):
        args = ['', 'options']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_version(self):
        args = ['', 'version']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_get_subscriptions(self):
        args = ['', 'get', 'subscriptions']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_generate_token(self):
        args = ['', 'generate', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_get_agents(self):
        args = ['', 'get', 'agents']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_get_channels(self):
        args = ['', 'get', 'channels']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        self.assertTrue(mock_print.call_count > 0)

    def test_print_help_config_invalid(self):
        args = ['', 'config', 'something']
        mock_printconfighelp = MagicMock()
        with patch('opentf.tools.ctl.print_config_help', mock_printconfighelp):
            opentf.tools.ctl.print_help(args)
        mock_printconfighelp.assert_called_once()

    def test_print_help_get_workflow(self):
        args = ['', 'get', 'workflow']
        mock_printworkflowhelp = MagicMock()
        with patch('opentf.tools.ctl.print_workflow_help', mock_printworkflowhelp):
            opentf.tools.ctl.print_help(args)
        mock_printworkflowhelp.assert_called_once()

    def test_print_help_get_workflows(self):
        args = ['', 'get', 'workflows']
        mock_printworkflowhelp = MagicMock()
        with patch('opentf.tools.ctl.print_workflow_help', mock_printworkflowhelp):
            opentf.tools.ctl.print_help(args)
        mock_printworkflowhelp.assert_called_once()

    def test_print_help_get_qualitygate(self):
        args = ['', 'get', 'qualitygate']
        mock_printqualitygatehelp = MagicMock()
        with patch(
            'opentf.tools.ctl.print_qualitygate_help', mock_printqualitygatehelp
        ):
            opentf.tools.ctl.print_help(args)
        mock_printqualitygatehelp.assert_called_once()

    def test_print_help_describe_qualitygate(self):
        args = ['', 'describe', 'qualitygate']
        mock_printqualitygatehelp = MagicMock()
        with patch(
            'opentf.tools.ctl.print_qualitygate_help', mock_printqualitygatehelp
        ):
            opentf.tools.ctl.print_help(args)
        mock_printqualitygatehelp.assert_called_once()

    def test_print_help_view_token(self):
        args = ['', 'view', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    def test_print_help_check_token(self):
        args = ['', 'check', 'token']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    def test_print_help_delete_subscription(self):
        args = ['', 'delete', 'subscription']
        mock_print = MagicMock()
        with patch('builtins.print', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    def test_print_help_cp(self):
        args = ['', 'cp']
        mock_printcphelp = MagicMock()
        with patch('opentf.tools.ctl.print_attachments_help', mock_printcphelp):
            opentf.tools.ctl.print_help(args)
        mock_printcphelp.assert_called_once()

    def test_print_help_get_attachments(self):
        args = ['', 'get', 'attachments']
        mock_print = MagicMock()
        with patch('opentf.tools.ctl.print_attachments_help', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    def test_print_help_generate_report(self):
        args = ['', 'generate', 'report']
        mock_print = MagicMock()
        with patch('opentf.tools.ctl.print_generate_report_help', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    def test_print_help_get_datasources(self):
        args = ['', 'get', 'datasource']
        mock_print = MagicMock()
        with patch('opentf.tools.ctl.print_get_datasources_help', mock_print):
            opentf.tools.ctl.print_help(args)
        mock_print.assert_called_once()

    # versions

    def test_get_tools_version_ok(self):
        mock_version = MagicMock(return_value='12.34.56')
        with patch('importlib.metadata.version', mock_version):
            opentf.tools.ctl.get_tools_version()
        mock_version.assert_called_once()

    def test_get_orchestrator_version_ok(self):
        bom_items = BOM['details']['items']
        mock_get = MagicMock(return_value=BOM)
        mock_print = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctl.CONFIG', CONFIG
        ):
            opentf.tools.ctl.get_orchestrator_version(False)
        mock_get.assert_called_once()
        mock_print.assert_called()
        self.assertEqual(mock_print.call_count, 5)
        print_calls = mock_print.call_args_list
        self.assertIn(bom_items['name'], print_calls[2][0][0])
        self.assertTrue(CONFIG['orchestrator']['server'] in print_calls[1][0][0])
        versions = [
            (item['name'], item['version'])
            for key, value in bom_items.items()
            for item in value
            if key != 'name'
        ]
        for call_nr in range(mock_print.call_count - 3):
            self.assertEqual(
                f'{versions[call_nr][0]}: {versions[call_nr][1]}',
                print_calls[call_nr + 3][0][1],
            )

    def test_get_orchestrator_version_KO(self):
        mock_get = MagicMock(
            return_value={'details': {'items': {'error': 'ErroR-eRROr'}}}
        )
        mock_error = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('opentf.tools.ctl._error', mock_error), patch(
            'opentf.tools.ctl.CONFIG', CONFIG
        ):
            opentf.tools.ctl.get_orchestrator_version(False)
            mock_get.assert_called_once()
            mock_error.assert_called_once_with('ErroR-eRROr')

    def test_get_orchestrator_version_debug(self):
        mock_get = MagicMock(return_value=BOM)
        mock_print = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctl.CONFIG', CONFIG
        ):
            opentf.tools.ctl.get_orchestrator_version(True)
        mock_get.assert_called()
        mock_print.assert_called_with(json.dumps(BOM['details']['items'], indent=2))

    # _make_hostport

    def test_make_hostport_noserver(self):
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_NOSERVER):
            self.assertRaises(SystemExit, _make_hostport, 'foo')

    def test_make_hostport_newlegacy(self):
        with patch('opentf.tools.ctlnetworking.CONFIG', CONFIG_NEW_LEGACY):
            self.assertRaises(SystemExit, _make_hostport, 'foo')

    # list_namespaces

    def test_list_namespace_noselector_star(self):
        mock_get = MagicMock(return_value={'details': {'items': ['*', 'namespace']}})
        mock_print = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('builtins.print', mock_print):
            opentf.tools.ctl.list_namespaces()
        mock_get.assert_called_once()
        mock_print.assert_called()
        self.assertEqual(mock_print.call_count, 2)

    def test_list_namespace_selector_star(self):
        mock_get = MagicMock(return_value={'details': {'items': ['*', 'namespace']}})
        mock_print = MagicMock()
        args = ['', '--selector', 'verb=create,resource==workflow']
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._get', mock_get
        ), patch('opentf.tools.ctl._observer'), patch('builtins.print', mock_print):
            opentf.tools.ctl.list_namespaces()
        mock_get.assert_called_once()
        mock_print.assert_called()
        self.assertEqual(mock_print.call_count, 2)

    def test_list_namespace_selector_toomany(self):
        args = ['', '--selector', 'verb=create,resource=workflow,verb=list']
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._fatal', mock_fatal
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_namespaces)
        mock_fatal.assert_called_once_with(
            'Invalid selector, must be of form: "resource==resource,verb==verb", got: %s.',
            'verb=create,resource=workflow,verb=list',
        )

    def test_list_namespace_selector_unknown(self):
        args = ['', '--selector', 'verb=create,ressource=workflow']
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._fatal', mock_fatal
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_namespaces)
        mock_fatal.assert_called_once_with(
            'Invalid selector, expecting "resource" or "verb", got: %s.',
            'ressource',
        )

    def test_list_namespace_selector_oneempty(self):
        args = ['', '--selector', 'verb=,resource=workflow']
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctl._fatal', mock_fatal
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_namespaces)
        mock_fatal.assert_called_once_with(
            'Incomplete selector, expecting both "resource" and "verb".'
        )

    def test_list_namespace_noselector_nostar(self):
        mock_get = MagicMock(return_value={'details': {'items': ['ns1', 'ns2']}})
        mock_print = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('builtins.print', mock_print):
            opentf.tools.ctl.list_namespaces()
        mock_get.assert_called_once()
        mock_print.assert_called()
        self.assertEqual(mock_print.call_count, 3)

    def test_list_namespace_badresponse(self):
        mock_get = MagicMock(return_value={})
        mock_print = MagicMock()
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._observer'
        ), patch('builtins.print', mock_print):
            self.assertRaises(SystemExit, opentf.tools.ctl.list_namespaces)

    # delete_subscription

    def test_delete_subscription_invalid_uuid(self):
        mock_delete = MagicMock(return_value={'message': 'deleTe SubSCRiption'})
        with patch('opentf.tools.ctl._delete', mock_delete), patch(
            'opentf.tools.ctl._eventbus'
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.delete_subscription, 'OhNo')
        mock_delete.assert_not_called()

    # delete_agent

    def test_delete_agent_invalid_uuid(self):
        mock_delete = MagicMock(return_value={'message': 'deleTe SubSCRiption'})
        with patch('opentf.tools.ctl._delete', mock_delete), patch(
            'opentf.tools.ctl._eventbus'
        ):
            self.assertRaises(SystemExit, opentf.tools.ctl.delete_agent, 'OhNo')
        mock_delete.assert_not_called()

    # _get_subscriptions2

    def test_get_subscriptions_noparams_ok(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._eventbus'
        ):
            what = opentf.tools.ctl._get_subscriptions()
        self.assertEqual(what, 'foo')

    def test_get_subscriptions_params_ok(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {'X-Processed-Query': 'fieldSelector'}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._eventbus'
        ):
            what = opentf.tools.ctl._get_subscriptions({'fieldselector': 'foo'})
        self.assertEqual(what, 'foo')

    def test_get_subscriptions_params_notsupported(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._eventbus'
        ):
            self.assertRaises(
                SystemExit,
                opentf.tools.ctl._get_subscriptions,
                {'fieldSelector': 'foo'},
            )

    # _get_agents

    def test_get_agents2_noparams_ok(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._agentchannel'
        ):
            what = opentf.tools.ctl._get_agents()
        self.assertEqual(what, 'foo')

    def test_get_agents2_noparams_ok_legacy(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': {'foo': {'metadata': {}}}}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._agentchannel'
        ):
            what = opentf.tools.ctl._get_agents()
        self.assertEqual(what, [{'metadata': {'agent_id': 'foo'}}])

    def test_get_agents2_params_ok(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {'X-Processed-Query': 'fieldSelector'}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._agentchannel'
        ):
            what = opentf.tools.ctl._get_agents({'fieldselector': 'foo'})
        self.assertEqual(what, 'foo')

    def test_get_agents2_params_notsupported(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'items': 'foo'}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctl._get', mock_get), patch(
            'opentf.tools.ctl._agentchannel'
        ):
            self.assertRaises(
                SystemExit,
                opentf.tools.ctl._get_agents,
                {'fieldSelector': 'foo'},
            )

    # _get_workflows

    def test_get_workflows_params_ok(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'items': 'foo'}}
        mock_response.headers = {'X-Processed-Query': 'fieldSelector'}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking._observer'
        ):
            what = _get_workflows({'fieldselector': 'foo'})
        self.assertEqual(what, 'foo')

    def test_get_workflows_params_notsupported(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'items': 'foo'}}
        mock_response.headers = {}
        mock_get = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking._observer'
        ):
            self.assertRaises(
                SystemExit,
                _get_workflows,
                {'fieldSelector': 'foo'},
            )

    def test_get_workflows_result_ko(self):
        mock_response = MagicMock()
        mock_response.json = MagicMock(side_effect=Exception('Boom!'))
        mock_response.headers = {'X-Processed-Query': 'fieldSelector'}
        mock_get = MagicMock(return_value=mock_response)
        mock_fatal = MagicMock(side_effect=SystemExit(2))
        with patch('opentf.tools.ctlnetworking._get', mock_get), patch(
            'opentf.tools.ctlnetworking._observer'
        ), patch('opentf.tools.ctlnetworking._fatal', mock_fatal):
            self.assertRaisesRegex(
                SystemExit, '2', _get_workflows, {'fieldselector': 'foo'}
            )
        mock_fatal.assert_called_once()
        self.assertEqual('Boom!', str(mock_fatal.call_args[0][1]))

    # handlers

    def test_handler_maybe_outdated_405(self):
        mockresponse = Response()
        mockresponse.status_code = 405
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctlnetworking._error', mock_error), patch(
            'opentf.tools.ctlnetworking._debug', mock_debug
        ):
            self.assertRaises(SystemExit, _handle_maybe_outdated, mockresponse)
        mock_error.assert_called_once()
        mock_debug.assert_called_once()

    def test_handler_maybe_outdated_500(self):
        mockresponse = Response()
        mockresponse.status_code = 500
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctlnetworking._error', mock_error), patch(
            'opentf.tools.ctlnetworking._debug', mock_debug
        ):
            self.assertRaises(SystemExit, _handle_maybe_outdated, mockresponse)
        mock_error.assert_called_once()
        mock_debug.assert_not_called()
