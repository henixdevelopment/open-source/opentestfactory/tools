# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""tools unit tests."""

import logging
import unittest
import sys

from unittest.mock import patch, MagicMock

from opentf.tools.ctlcommons import (
    _make_params_from_selectors,
    _is_command,
    _get_arg,
    _get_args,
    _get_columns,
    _ensure_uuid,
    _ensure_options,
    _get_jsonpath,
    _generate_row,
    _file_not_found,
)


########################################################################
# Datasets

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_OK2 = '7920ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_OK_PARTIAL = '6120'

########################################################################
# Helpers


class TestCtlCommons(unittest.TestCase):
    """Unit tests for ctlcommons."""

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # _is_command

    def test_is_command_okany(self):
        result = _is_command('_', ['', 'foo'])
        self.assertTrue(result)

    def test_is_command_okfoo(self):
        result = _is_command('foo', ['', 'foo'])
        self.assertTrue(result)

    def test_is_command_missingbar(self):
        result = _is_command('foo bar', ['', 'foo'])
        self.assertFalse(result)

    def test_is_command_okmaybemissing(self):
        self.assertRaises(SystemExit, _is_command, '_ _', ['', 'foo'])

    def test_is_command_5(self):
        result = _is_command('_ foo', ['', 'foo'])
        self.assertFalse(result)

    def test_is_command_6(self):
        result = _is_command('_ foo', ['', 'bar', 'foo'])
        self.assertTrue(result)

    def test_is_command_7(self):
        result = _is_command('_ bar', ['', 'bar', 'foo'])
        self.assertFalse(result)

    def test_is_command_star_none(self):
        result = _is_command('delete foo *', ['', 'delete', 'foo'])
        self.assertTrue(result)

    def test_is_command_star_multi(self):
        result = _is_command('delete foo *', ['', 'delete', 'foo', 'bar'])
        self.assertTrue(result)

    def test_is_command_star(self):
        result = _is_command('delete foo *', ['', 'delete', 'foo', 'bar', 'baz'])
        self.assertTrue(result)

    # _get_value

    def test_get_value_1(self):
        args = ['', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada=')
        self.assertIsNone(result)

    def test_get_value_2(self):
        args = ['', '--yada=bar']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada=')
        self.assertEqual(result, 'bar')

    def test_get_value_3(self):
        args = ['', '--yada=bar', '--yada=baz']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada=')
        self.assertEqual(result, 'bar')

    def test_get_value_4(self):
        args = ['', '--yada_foo=1']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada-foo=')
        self.assertEqual(result, '1')

    def test_get_value_5(self):
        args = ['', '--yada-foo=1']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada-foo=')
        self.assertEqual(result, '1')

    def test_get_value_5alternate(self):
        args = ['', '--yada-foo', '1']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada-foo=')
        self.assertEqual(result, '1')

    def test_get_value_6(self):
        args = ['', '--yada-foo']
        with patch.object(sys, 'argv', args):
            result = _get_arg('--yada-foo=')
        self.assertIsNone(result)

    # _get_args

    def test_get_values_1(self):
        args = ['', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_args('--yada=')
        self.assertEqual([], result)

    def test_get_values_2(self):
        args = ['', '--yada=bar', '--yada=foo']
        with patch.object(sys, 'argv', args):
            result = _get_args('--yada=')
        self.assertEqual(['bar', 'foo'], result)

    def test_get_values_3(self):
        args = ['', '--yada', 'bar', '--yada', 'foo']
        with patch.object(sys, 'argv', args):
            result = _get_args('--yada=')
        self.assertEqual(['bar', 'foo'], result)

    # _get_columns

    def test_get_columns_1(self):
        args = ['']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__DEFAULT__')

    def test_get_columns_2(self):
        args = ['', '-o', 'wide']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__WIDE__')

    def test_get_columns_3(self):
        args = ['', '--output=wide', 'yada']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__WIDE__')

    def test_get_columns_4(self):
        args = ['', '-o', 'notwide']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, '__DEFAULT__')

    def test_get_columns_5(self):
        args = ['', 'custom-columns=yada']
        with patch.object(sys, 'argv', args):
            self.assertRaises(
                ValueError, _get_columns, wide='__WIDE__', default='__DEFAULT__'
            )

    def test_get_columns_6(self):
        args = ['', '--output=custom-columns=yada']
        with patch.object(sys, 'argv', args):
            self.assertRaises(
                ValueError, _get_columns, wide='__WIDE__', default='__DEFAULT__'
            )

    def test_get_columns_7(self):
        args = ['', '--output=custom-columns=yada:yoda']
        with patch.object(sys, 'argv', args):
            result = _get_columns(wide='__WIDE__', default='__DEFAULT__')
        self.assertEqual(result, ['yada:yoda'])

    # _ensure_uuid

    def test_ensure_uuid_1(self):
        self.assertRaises(SystemExit, _ensure_uuid, 'not an uuid')

    def test_ensure_uuid_2(self):
        self.assertEqual(_ensure_uuid(UUID_OK), UUID_OK)

    def test_ensure_uuid_partial(self):
        self.assertRaises(SystemExit, _ensure_uuid, UUID_OK_PARTIAL)

    def test_ensure_uuid_complete(self):
        mock_complete = MagicMock(return_value=[UUID_OK, UUID_OK2])
        what = _ensure_uuid(UUID_OK_PARTIAL, mock_complete)
        mock_complete.assert_called_once_with()
        self.assertEqual(what, UUID_OK)

    def test_ensure_uuid_complete_notfound(self):
        mock_complete = MagicMock(return_value=[UUID_OK2])
        self.assertRaises(SystemExit, _ensure_uuid, UUID_OK_PARTIAL, mock_complete)
        mock_complete.assert_called_once_with()

    def test_ensure_uuid_complete_ambiguous(self):
        mock_complete = MagicMock(return_value=[UUID_OK, UUID_OK])
        self.assertRaises(SystemExit, _ensure_uuid, UUID_OK_PARTIAL, mock_complete)
        mock_complete.assert_called_once_with()

    # _ensure_options

    def test_ensure_options_missing_parameter(self):
        self.assertRaises(SystemExit, _ensure_options, 'cmd', ['cmd', '--user'])

    def test_ensure_options_parameter_ok(self):
        self.assertIsNone(_ensure_options('cmd', ['cmd', '--user', 'foo']))

    def test_ensure_options_flag_ok(self):
        self.assertIsNone(
            _ensure_options('foo', ['foo', '--flag'], flags=(('--flag',),))
        )

    def test_ensure_options_unexpected_value_for_flag(self):
        self.assertRaises(
            SystemExit,
            _ensure_options,
            'cmd',
            ['cmd', '--flag', 'foo'],
            flags=(('--flag',),),
        )

    def test_ensure_options_unexpected_syntax_for_flag(self):
        self.assertRaises(
            SystemExit,
            _ensure_options,
            'cmd',
            ['cmd', '--flag=1'],
            flags=(('--flag',),),
        )

    # _make_params_from_selectors

    def test_make_selectors_field(self):
        args = ['', '--field-selector', 'one=two']
        exp_result = {'fieldSelector': 'one=two'}
        with patch.object(sys, 'argv', args):
            result = _make_params_from_selectors()
        self.assertEqual(exp_result, result)

    def test_make_selectors_label(self):
        args = ['', '--selector', 'label=lAbEl']
        exp_result = {'labelSelector': 'label=lAbEl'}
        with patch.object(sys, 'argv', args):
            result = _make_params_from_selectors()
        self.assertEqual(exp_result, result)

    def test_make_selectors_field_and_label(self):
        args = ['', '-l', 'label=lAbEl', '--field-selector', 'three in (four,five)']
        exp_result = {
            'labelSelector': 'label=lAbEl',
            'fieldSelector': 'three in (four,five)',
        }
        with patch.object(sys, 'argv', args):
            result = _make_params_from_selectors()
        self.assertEqual(exp_result, result)

    def test_make_selectors_no_selectors(self):
        args = ['', '-o', '-p', '-x']
        with patch.object(sys, 'argv', args):
            result = _make_params_from_selectors()
        self.assertEqual({}, result)

    # _get_jsonpath

    def test_get_jsonpath_nopath(self):
        arg = ['a', 'b', 'c']
        self.assertEqual(arg, _get_jsonpath(arg, []))

    def test_get_jsonpath_noobject(self):
        arg = 'foobar'
        self.assertIsNone(_get_jsonpath(arg, ['abc']))

    def test_get_jsonpath_keys(self):
        arg = {'a': 12, 'b': 34}
        self.assertEqual(['a', 'b'], _get_jsonpath(arg, ['*~']))

    def test_get_jsonpath_flatten(self):
        args = {
            'job1': {'labels': {'label11': 10, 'label12': 11}},
            'job2': {'labels': {'label21': 20, 'label22': 21}},
        }
        exp_result = [10, 11, 20, 21]
        self.assertEqual(exp_result, _get_jsonpath(args, ['*', 'labels', '*']))

    def test_get_jsonpath_flatten_keys(self):
        args = {
            'job1': {'labels': {'label11': 10, 'label12': 11}},
            'job2': {'labels': {'label21': 20, 'label22': 21}},
        }
        exp_result = ['label11', 'label12', 'label21', 'label22']
        self.assertEqual(exp_result, _get_jsonpath(args, ['*', 'labels', '*~']))

    def test_get_jsonpath_noflatten(self):
        args = {
            'job1': {'labels': ['label11', 'label12']},
            'job2': {'labels': ['label21', 'label22']},
        }
        exp_result = [['label11', 'label12'], ['label21', 'label22']]
        self.assertEqual(exp_result, _get_jsonpath(args, ['*', 'labels']))

    def test_get_jsonpath_list_noflatten(self):
        args = [
            {'labels': ['label11', 'label12']},
            {'labels': ['label21', 'label22']},
        ]
        exp_result = [['label11', 'label12'], ['label21', 'label22']]
        self.assertEqual(exp_result, _get_jsonpath(args, ['*', 'labels']))

    def test_get_jsonpath_list_flatten(self):
        args = [
            {'labels': ['label11', 'label12']},
            {'labels': ['label21', 'label22']},
        ]
        exp_result = ['label11', 'label12', 'label21', 'label22']
        self.assertEqual(exp_result, _get_jsonpath(args, ['*', 'labels', '*']))

    # _generate_row

    def test_generate_row_nok(self):
        bad_columns = ['TITLE:.metadata.title', 'BAD:.metadata.*~.ohno']
        self.assertRaises(SystemExit, _generate_row, None, bad_columns)

    # _file_not_found

    def test_file_not_found(self):
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctlcommons._error', mock_error), patch(
            'opentf.tools.ctlcommons._debug', mock_debug
        ):
            self.assertRaisesRegex(
                SystemExit, '2', _file_not_found, 'FiLe', 'NoT FoUnd'
            )
        mock_error.assert_called_once_with('File not found: %s.', 'FiLe')
        mock_debug.assert_called_once_with('Error is: %s.', 'NoT FoUnd')
