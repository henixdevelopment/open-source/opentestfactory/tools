# Copyright (c) 2023-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch, mock_open

from opentf.tools import ctlattachments

########################################################################
# Datasets

FIRST_UUID = '3f06a05d-4f06-4a03-a6ba-84e6d48ab678'
SECOND_UUID = '2c3da007-d29c-496c-b6e7-a2f07055866a'
WF_UUID = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'

WORKFLOW_ID_EVENTS_OK = 'b10a8635-0d36-4f40-8776-d9ac741e1b0f'
ATTACHMENT_EVENTS_OK = [
    {
        'kind': 'Workflow',
        'jobs': {
            'job1': {
                'name': 'Most Fabulous Job',
                'steps': [
                    {
                        'id': 'job1_step3',
                        'uses': 'aNotherTechnology/execute@v12',
                        'with': {'test': 'some/large/test.test'},
                    },
                    {
                        'id': 'job1_step1',
                        'uses': 'smartTechnology/execute@v10',
                        'with': {'test': 'some/small/test.st'},
                    },
                    {
                        'id': 'job1_step2',
                        'uses': 'NoTsmartTechnology/execute@v11',
                        'with': {'test': 'some/small/unittest.st.st'},
                    },
                ],
            }
        },
    },
    {
        'kind': 'ExecutionCommand',
        'metadata': {
            'step_sequence_id': -1,
            'job_id': 'job1',
            'name': 'Most Fabulous Job',
        },
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {
                '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html': {
                    'uuid': '5b444910-a246-4d66-b02b-40e77148b90d',
                    'type': 'some-file-type',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 32,
            'step_origin': ['job1_step1', 'job0_step3'],
        },
        'status': 0,
        'logs': ['executionresult_2_log'],
        'attachments': [
            '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html'
        ],
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {
                '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-result.tar.gz': {
                    'uuid': '5c444910-a246-4d66-b02b-40e77148b90d',
                    'type': 'some-other-file-type',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 33,
            'step_origin': ['job1_step2', 'job0_step3'],
        },
        'status': 0,
        'logs': ['executionresult_2_log'],
        'attachments': [
            '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_fe28339a-6e76-4478-8a75-1e27a29cfe17-result.tar.gz'
        ],
    },
    {
        'kind': 'WorkflowResult',
        'metadata': {
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'workflow_id': 'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
        },
        'attachments': [
            '/tmp/allureReporting/b10a8635-0d36-4f40-8776-d9ac741e1b0f/allure-report.tar'
        ],
    },
    {
        'kind': 'WorkflowResult',
        'metadata': {
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'attachments': {
                '/tmp/b10a8635-0d36-4f40-8776-d9ac741e1b0f-af0edfc3-6cc6-4a25-912b-e11d1d09b752_WR_executionlog.txt': {
                    'type': 'tYpe',
                    'uuid': 'af0edfc3-6cc6-4a25-912b-e11d1d09b752',
                }
            },
            'workflow_id': 'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
        },
        'attachments': [
            '/tmp/b10a8635-0d36-4f40-8776-d9ac741e1b0f-af0edfc3-6cc6-4a25-912b-e11d1d09b752_WR_executionlog.txt'
        ],
    },
]

ATTACHMENT_EVENTS_KO = [
    {
        'kind': 'Workflow',
        'jobs': {
            'job1': {
                'name': 'Most Fabulous Job',
                'steps': [
                    {
                        'id': 'job1_step3',
                        'uses': 'aNotherTechnology/execute@v12',
                        'with': {'test': 'some/large/test.test'},
                    },
                ],
            }
        },
    },
    {
        'kind': 'ExecutionCommand',
        'metadata': {
            'step_sequence_id': -1,
            'job_id': 'job1',
            'name': 'Most Fabulous Job',
        },
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {
                '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_nonexistent_attachment.html': {
                    'uuid': '5b444910-a246-4d66-b02b-40e77148b90d',
                    'type': 'some-file-type',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 32,
            'step_origin': ['job1_step3', 'job0_step3'],
        },
        'status': 2,
        'logs': ['Error-error'],
        'attachments': [
            '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_nonexistent_attachment.html'
        ],
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {
                '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_nonexistent_attachment.html': {
                    'uuid': '6b444910-a246-4d66-b02b-40e77148b90d',
                    'type': 'some-file-type',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 33,
            'step_origin': ['job1_step3', 'job0_step3'],
        },
        'status': 1,
        'logs': ['Error-error-error'],
        'attachments': [
            '/tmp/e5867898-d9fe-45ac-9901-ab801f009e41-dfdd5925-7036-4f65-ae5b-e2515cbd7a53_63_nonexistent_attachment.html'
        ],
    },
]

ATTACHMENT_EVENTS_WITH_HOOK = [
    {
        'kind': 'Workflow',
        'jobs': {
            'job1': {
                'steps': [
                    {
                        'id': 'job1_step1',
                        'uses': 'smartTechnology/execute@v10',
                        'with': {'test': 'some/small/test.st'},
                    },
                    {
                        'id': 'job1_step2',
                        'uses': 'NoTsmartTechnology/execute@v10',
                        'with': {'test': 'some/small/unittest.st.st'},
                    },
                ]
            }
        },
    },
    {
        'kind': 'ExecutionCommand',
        'metadata': {
            'step_sequence_id': -1,
            'job_id': 'job1',
            'name': 'Most Fabulous Job',
        },
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'annotations': {'opentestfactory.org/hooks': {'channel': 'setup'}},
            'attachments': {
                '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2f_0_bar.txt': {
                    'uuid': '3d0b6138-4f0c-4416-92ed-9820629bcf2f',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 33,
            'step_sequence_id': 454,
            'step_origin': [],
        },
        'status': 123,
        'attachments': [
            '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2f_0_bar.txt'
        ],
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'annotations': {'opentestfactory.org/hooks': {'channel': 'teardown'}},
            'attachments': {
                '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2e_1_bar.jpg': {
                    'uuid': '3d0b6138-4f0c-4416-92ed-9820629bcf2e',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 34,
            'step_sequence_id': 455,
            'step_origin': [],
        },
        'status': 123,
        'attachments': [
            '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2e_1_bar.jpg'
        ],
    },
]

WORKFLOW_RESULTS = [
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "attachments": [
            "/tmp/054ccc00-a018-4d54-902a-31c283a095b5-a79e0e0f-32ca-41cc-88d1-e93015bb010b_WR_justxml.xml"
        ],
        "kind": "WorkflowResult",
        "metadata": {
            "attachments": {
                "/tmp/054ccc00-a018-4d54-902a-31c283a095b5-a79e0e0f-32ca-41cc-88d1-e93015bb010b_WR_justxml.xml": {
                    "type": "application/vnd.opentestfactory.executionreport+xml",
                    "uuid": "uuid1",
                }
            },
            "creationTimestamp": "2024-09-23T16:35:30.316320",
            "name": "justxml",
            "namespace": "default",
            "request_id": None,
            "workflow_id": "054ccc00-a018-4d54-902a-31c283a095b5",
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "attachments": [
            "/tmp/054ccc00-a018-4d54-902a-31c283a095b5-101fa5e9-fd08-47a6-95f9-8e9478999dbb_WR_justlog.txt"
        ],
        "kind": "WorkflowResult",
        "metadata": {
            "attachments": {
                "/tmp/054ccc00-a018-4d54-902a-31c283a095b5-101fa5e9-fd08-47a6-95f9-8e9478999dbb_WR_justlog.txt": {
                    "type": "application/vnd.opentestfactory.executionlog+text",
                    "uuid": "uuid2",
                }
            },
            "creationTimestamp": "2024-09-23T16:35:30.332271",
            "name": "justlog",
            "namespace": "default",
            "request_id": None,
            "workflow_id": "054ccc00-a018-4d54-902a-31c283a095b5",
        },
    },
]

REUSABLE_WORKFLOW = [
    {
        'kind': 'GeneratorResult',
        'metadata': {'download_job': True, 'name': '.download_job_caller-job'},
        'jobs': {
            'job1': {
                'steps': [
                    {
                        'id': 'job1_step1',
                        'uses': 'smartTechnology/execute@v10',
                        'with': {'test': 'some/small/test.st'},
                    },
                    {
                        'id': 'job1_step2',
                        'uses': 'NoTsmartTechnology/execute@v10',
                        'with': {'test': 'some/small/unittest.st.st'},
                    },
                ]
            }
        },
    },
    {
        'kind': 'ExecutionCommand',
        'metadata': {
            'step_sequence_id': -1,
            'job_id': 'job1',
            'name': '.download_job_caller-job',
        },
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {
                '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2f_0_bar.txt': {
                    'uuid': '3d0b6138-4f0c-4416-92ed-9820629bcf2f',
                }
            },
            'name': 'nAMe',
            'creationTimestamp': '1970-01-01',
            'job_id': 'job1',
            'step_id': 33,
            'step_sequence_id': 454,
            'step_origin': [],
        },
        'status': 123,
        'attachments': [
            '/tmp/d02e2135-aa0f-4361-83ef-bf33ee8f2d75-3d0b6138-4f0c-4416-92ed-9820629bcf2f_0_bar.txt'
        ],
    },
]


########################################################################


class TestCtlAttachments(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # download attachment

    def test_download_attachment_ok(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {
            'Content-Disposition': f'filename={FIRST_UUID}_report.html'
        }
        mock_get_file = MagicMock(return_value=mock_response)
        mock_write = mock_open()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'builtins.open', mock_write
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ):
            ctlattachments.download_attachment('wf_id', FIRST_UUID, '/file/path/x.y')
        mock_get_file.assert_called_once()
        self.assertEqual(
            f'/workflows/wf_id/files/{FIRST_UUID}', mock_get_file.call_args[0][1]
        )
        mock_write.assert_called_once_with('/file/path/x.y', 'wb')
        mock_print.assert_called_once_with(
            f'Attachment report.html ({FIRST_UUID}) is downloaded at /file/path/x.y.'
        )

    def test_download_attachment_untitled_attachment(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {}
        mock_get_file = MagicMock(return_value=mock_response)
        mock_write = mock_open()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'builtins.open', mock_write
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ):
            ctlattachments.download_attachment('wf_id', FIRST_UUID, '/file/path/x.y')
        mock_print.assert_called_once_with(
            f'Attachment untitled ({FIRST_UUID}) is downloaded at /file/path/x.y.'
        )

    def test_download_attachment_with_filename(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {
            'Content-Disposition': f'filename={FIRST_UUID}_second_report.html'
        }
        mock_get_file = MagicMock(return_value=mock_response)
        mock_write = mock_open()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'builtins.open', mock_write
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ), patch(
            'os.path.isdir', return_value=True
        ):
            ctlattachments.download_attachment(
                'wF', FIRST_UUID, '/dir/dir2/dir3', 'custom_report_name'
            )
        mock_print.assert_called_once_with(
            f'Attachment second_report.html ({FIRST_UUID}) is downloaded at /dir/dir2/dir3/custom_report_name.'
        )

    def test_download_attachment_ko_404(self):
        mock_response = MagicMock()
        mock_response.status_code = 404
        mock_response.json = lambda: {'message': 'Not found. 404. Error.'}
        mock_get_file = MagicMock(return_value=mock_response)
        mock_save_att = MagicMock()
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ), patch('opentf.tools.ctlattachments._save_attachment_stream', mock_save_att):
            self.assertRaises(
                SystemExit,
                ctlattachments.download_attachment,
                'ze_wf_id',
                FIRST_UUID,
                'ze/file/path.txt',
            )
        mock_get_file.assert_called_once()
        mock_save_att.assert_not_called()

    def test_download_attachment_ko_403(self):
        mock_response = MagicMock()
        mock_response.status_code = 403
        mock_response.json = lambda: {'message': 'Forbidden. 403. Error-error.'}
        mock_get_file = MagicMock(return_value=mock_response)
        mock_save_att = MagicMock()
        mock_fatal = MagicMock(side_effect=SystemExit('Oops'))
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ), patch(
            'opentf.tools.ctlattachments._save_attachment_stream', mock_save_att
        ), patch(
            'opentf.tools.ctlattachments._fatal', mock_fatal
        ):
            self.assertRaises(
                SystemExit,
                ctlattachments.download_attachment,
                'a_wf_id',
                FIRST_UUID,
                'a/file/path.tar',
            )
        mock_get_file.assert_called_once()
        mock_save_att.assert_not_called()
        mock_fatal.assert_called_once()
        fatal_msg = mock_fatal.call_args[0]
        self.assertEqual(
            f'Failed to get attachment {FIRST_UUID} from localstore', fatal_msg[1]
        )
        self.assertEqual('Forbidden. 403. Error-error.', fatal_msg[3])

    def test_download_attachment_destination_ko(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.content = 'abcdef'
        mock_get_file = MagicMock(return_value=mock_response)
        mock_write = MagicMock(side_effect=FileNotFoundError('Oops'))
        mock_fatal = MagicMock(side_effect=SystemExit('Whoops'))
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'builtins.open', mock_write
        ), patch('os.path.isdir', MagicMock(return_value=False)), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ), patch(
            'opentf.tools.ctlattachments._fatal', mock_fatal
        ), patch(
            'opentf.tools.ctlattachments._get_download_name',
            MagicMock(return_value='name'),
        ):
            self.assertRaises(
                SystemExit,
                ctlattachments.download_attachment,
                'wf_id',
                FIRST_UUID,
                '/no/such/file/path/.foo',
            )
        mock_get_file.assert_called_once()
        mock_write.assert_called_once()
        mock_fatal.assert_called_once()
        fatal_msg = mock_fatal.call_args[0]
        self.assertIn('Failed to download attachment', fatal_msg[0])
        self.assertEqual(FIRST_UUID, fatal_msg[1])
        self.assertEqual('/no/such/file/path/.foo', fatal_msg[2])
        self.assertEqual('Oops', fatal_msg[3])

    def test_download_attachment_destination_is_dir(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {
            'Content-Disposition': f'filename={FIRST_UUID}_output.xml'
        }
        mock_get_file = MagicMock(return_value=mock_response)
        mock_write = mock_open()
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._get_file', mock_get_file), patch(
            'builtins.open', mock_write
        ), patch('builtins.print', mock_print), patch(
            'os.path.isdir', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlattachments._localstore', MagicMock()
        ):
            ctlattachments.download_attachment(
                'wf_id', FIRST_UUID, '/file/path/dir/dir'
            )
        mock_get_file.assert_called_once()
        self.assertEqual(
            f'/workflows/wf_id/files/{FIRST_UUID}', mock_get_file.call_args[0][1]
        )
        mock_write.assert_called_once_with(f'/file/path/dir/dir/output.xml', 'wb')
        mock_print.assert_called_once_with(
            f'Attachment output.xml ({FIRST_UUID}) is downloaded at /file/path/dir/dir/output.xml.'
        )

    # download multiple attachments

    def test_download_attachments_all(self):
        workflow_id = WORKFLOW_ID_EVENTS_OK
        mock_gad = MagicMock(return_value=ATTACHMENT_EVENTS_OK)
        mock_da = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events', mock_gad
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'os.path.exists', MagicMock(return_value=True)
        ):
            ctlattachments.download_attachments(workflow_id, '*', '/some/file/path')
        mock_gad.assert_called_once()
        self.assertEqual(4, mock_da.call_count)
        calls = mock_da.call_args_list
        self.assertEqual(
            (
                workflow_id,
                '5b444910-a246-4d66-b02b-40e77148b90d',
                '/some/file/path/Most Fabulous Job/2_test.st',
                'fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html',
            ),
            calls[0][0],
        )
        self.assertEqual(
            (
                workflow_id,
                '5c444910-a246-4d66-b02b-40e77148b90d',
                '/some/file/path/Most Fabulous Job/3_unittest.st.st',
                'fe28339a-6e76-4478-8a75-1e27a29cfe17-result.tar.gz',
            ),
            calls[1][0],
        )
        self.assertEqual(
            (
                workflow_id,
                'b10a8635-0d36-4f40-8776-d9ac741e1b0f',
                '/some/file/path',
                'allure-report.tar',
            ),
            calls[2][0],
        )
        self.assertEqual(
            (
                workflow_id,
                'af0edfc3-6cc6-4a25-912b-e11d1d09b752',
                '/some/file/path',
                'executionlog.txt',
            ),
            calls[3][0],
        )

    def test_download_attachments_only_txt(self):
        mock_gad = MagicMock(return_value=ATTACHMENT_EVENTS_WITH_HOOK)
        mock_da = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events', mock_gad
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'os.path.exists', MagicMock(return_value=False)
        ), patch(
            'os.makedirs', MagicMock()
        ), patch(
            'opentf.tools.ctlattachments._file_exists', MagicMock(return_value=False)
        ):
            ctlattachments.download_attachments(
                'WfId', '*.txt', '/some/other/file/path'
            )
        mock_gad.assert_called_once()
        mock_da.assert_called_once_with(
            'WfId',
            '3d0b6138-4f0c-4416-92ed-9820629bcf2f',
            '/some/other/file/path/Most Fabulous Job/setup',
            '454_bar.txt',
        )

    def test_download_attachments_all_reusable(self):
        mock_gad = MagicMock(return_value=REUSABLE_WORKFLOW)
        mock_da = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events', mock_gad
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'os.path.exists', MagicMock(return_value=False)
        ), patch(
            'os.makedirs', MagicMock()
        ), patch(
            'opentf.tools.ctlattachments._file_exists', MagicMock(return_value=False)
        ):
            ctlattachments.download_attachments('WfId', '*', '/some/other/file/path')
        mock_gad.assert_called_once()
        mock_da.assert_called_once_with(
            'WfId',
            '3d0b6138-4f0c-4416-92ed-9820629bcf2f',
            '/some/other/file/path/caller-job/download_job_caller-job',
            '454_bar.txt',
        )

    def test_download_attachments_all_filter_on_type(self):
        mock_gad = MagicMock(return_value=ATTACHMENT_EVENTS_OK)
        mock_da = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events', mock_gad
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlattachments._get_arg', MagicMock(return_value='*file-type')
        ):
            ctlattachments.download_attachments(
                WORKFLOW_ID_EVENTS_OK, '*', '/some/file/path'
            )
        mock_gad.assert_called_once()
        self.assertEqual(2, mock_da.call_count)

    def test_download_attachments_no_matching_pattern_on_type(self):
        mock_gad = MagicMock(return_value=ATTACHMENT_EVENTS_OK)
        mock_da = MagicMock()
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events', mock_gad
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'os.path.exists', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlattachments._get_arg', MagicMock(return_value='*xml*')
        ), patch(
            'opentf.tools.ctlattachments._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit,
                '1',
                ctlattachments.download_attachments,
                WORKFLOW_ID_EVENTS_OK,
                '*',
                '/some/file/path',
            )
        mock_gad.assert_called_once()
        mock_da.assert_not_called()
        mock_error.assert_called_once_with('No attachment matching pattern found.')

    def test_download_attachments_ko_destination_is_file(self):
        mock_error = MagicMock()
        with patch('os.path.isfile', MagicMock(return_value=True)), patch(
            'opentf.tools.ctlattachments._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit,
                '1',
                ctlattachments.download_attachments,
                'WfId',
                '*',
                '/some/file/path',
            )
        mock_error.assert_called_once_with(
            'File path `%s` is a file, not directory. Can not download multiple attachments to a file, aborting.',
            '/some/file/path',
        )

    # download_workflow_reports

    def test_download_workflow_reports_ok_idle(self):
        with patch('opentf.tools.ctlattachments.time', side_effect=[1, 2]), patch(
            'opentf.tools.ctlattachments.sleep'
        ) as mock_sleep, patch(
            'opentf.tools.ctlattachments._get_workers_status',
            side_effect=['BUSY', 'IDLE'],
        ), patch(
            'opentf.tools.ctlattachments._do_download_reports'
        ) as mock_reports:
            ctlattachments.download_workflow_reports('WF3', {'report1': {'path2'}})
        self.assertEqual(2, mock_sleep.call_count)
        mock_reports.assert_called_once_with('WF3', {'report1': {'path2'}})

    def test_download_workflow_reports_ok_busy(self):
        with patch('opentf.tools.ctlattachments.time', side_effect=[1, 2, 67]), patch(
            'opentf.tools.ctlattachments.sleep'
        ) as mock_sleep, patch(
            'opentf.tools.ctlattachments._get_workers_status',
            return_value='BUSY',
        ), patch(
            'opentf.tools.ctlattachments._do_download_reports'
        ) as mock_reports, patch(
            'opentf.tools.ctlattachments._warning'
        ) as mock_warning:
            ctlattachments.download_workflow_reports('WF4', {'report3': {'path4'}})
        self.assertEqual(2, mock_sleep.call_count)
        mock_reports.assert_called_once_with('WF4', {'report3': {'path4'}})
        mock_warning.assert_called_once()
        self.assertIn(
            'Timeout while waiting for worker completion', mock_warning.call_args[0][0]
        )

    def test_download_workflow_reports_ko_no_reports(self):
        with patch('opentf.tools.ctlattachments._error') as mock_error:
            self.assertRaisesRegex(
                SystemExit, '1', ctlattachments.download_workflow_reports, 'WF1', {}
            )
        mock_error.assert_called_once_with('No report to download, aborting.')

    def test_download_workflow_reports_ko_exception(self):
        with patch(
            'opentf.tools.ctlattachments.sleep', side_effect=Exception('Boo!')
        ), patch('opentf.tools.ctlattachments._error') as mock_error:
            ctlattachments.download_workflow_reports('WF2', {'report': {'path'}})
        mock_error.assert_called_once_with(
            'Internal error while downloading reports: %s.', 'Boo!'
        )

    # _get_workers_status

    def test_get_workers_status_ok(self):
        response = {'details': {'status': 'foo'}}
        with patch(
            'opentf.tools.ctlattachments._get', return_value=response
        ) as mock_get, patch('opentf.tools.ctlattachments._observer'):
            res = ctlattachments._get_workers_status('WF1')
        mock_get.assert_called_once()
        self.assertEqual('foo', res)

    # _do_download_reports

    def test_do_download_reports_ok(self):
        response = {'details': {'items': WORKFLOW_RESULTS}}
        with patch('opentf.tools.ctlattachments.sleep'), patch(
            'opentf.tools.ctlattachments._get', return_value=response
        ) as mock_get, patch('opentf.tools.ctlattachments._observer'), patch(
            'os.path.exists', side_effect=[True, False]
        ), patch(
            'os.makedirs'
        ) as mock_dir, patch(
            'opentf.tools.ctlattachments.download_attachment'
        ) as mock_download:
            ctlattachments._do_download_reports(
                'WF1',
                {'justlog.txt': {'.'}, 'justxml.xml': {'/some/other/path/xml.xml'}},
            )
        mock_get.assert_called_once()
        mock_dir.assert_called_once_with('/some/other/path')
        self.assertEqual(2, mock_download.call_count)
        print(mock_download.call_args_list)
        mock_download.assert_any_call('WF1', 'uuid2', '.', 'justlog.txt')
        mock_download.assert_any_call('WF1', 'uuid1', '/some/other/path', 'xml.xml')

    def test_do_download_reports_ko_no_results(self):
        response = {'details': {'items': []}}
        with patch('opentf.tools.ctlattachments.sleep'), patch(
            'opentf.tools.ctlattachments._get', return_value=response
        ) as mock_get, patch('opentf.tools.ctlattachments._observer'), patch(
            'opentf.tools.ctlattachments._error'
        ) as mock_error:
            self.assertRaisesRegex(
                SystemExit,
                '1',
                ctlattachments._do_download_reports,
                'WF2',
                {'justlog.txt': {'.'}, 'justxml.xml': {'/some/other/path/xml.xml'}},
            )
        mock_get.assert_called_once()
        mock_error.assert_called_once_with(
            'Failed to get WorkflowResult events from observer.'
        )

    def test_do_download_reports_report_not_found(self):
        response = {'details': {'items': WORKFLOW_RESULTS}}
        with patch('opentf.tools.ctlattachments.sleep'), patch(
            'opentf.tools.ctlattachments._get', return_value=response
        ) as mock_get, patch('opentf.tools.ctlattachments._observer'), patch(
            'opentf.tools.ctlattachments._warning'
        ) as mock_warning:
            ctlattachments._do_download_reports('WF3', {'somereport.txt': {'somepath'}})
        mock_get.assert_called_once()
        mock_warning.assert_called_once_with(
            'Report %s not found in workflow results, cannot download.',
            'somereport.txt',
        )

    # _file_exists

    def test_file_exists_true_200(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.headers = {'File-Exists': 'True'}
        mock_head = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctlattachments._head', mock_head), patch(
            'opentf.tools.ctlattachments._localstore'
        ):
            response = ctlattachments._file_exists('AttId', 'WfId')
        self.assertTrue(response)

    def test_file_exists_false_404(self):
        mock_response = MagicMock()
        mock_response.status_code = 404
        mock_head = MagicMock(return_value=mock_response)
        with patch('opentf.tools.ctlattachments._head', mock_head), patch(
            'opentf.tools.ctlattachments._localstore'
        ):
            response = ctlattachments._file_exists('AttId', 'WfId')
        self.assertFalse(response)

    # list attachments

    def test_list_attachments_ok(self):
        workflow_id = WORKFLOW_ID_EVENTS_OK
        mock_output = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._ensure_uuid',
            MagicMock(return_value=workflow_id),
        ), patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_OK),
        ), patch(
            'opentf.tools.ctlattachments.generate_output', mock_output
        ):
            ctlattachments.list_attachments(workflow_id, False)
        mock_output.assert_called_once()
        call = mock_output.call_args[0][0]
        self.assertEqual(4, len(call))
        first_result = call[0]['attachment']
        self.assertEqual('5b444910-a246-4d66-b02b-40e77148b90d', first_result['uuid'])
        self.assertEqual(
            'fe28339a-6e76-4478-8a75-1e27a29cfe17-attachment.html',
            first_result['filename'],
        )
        self.assertEqual('some-file-type', first_result['type'])
        second_result = call[1]['attachment']
        self.assertEqual('Most Fabulous Job', second_result['job']['name'])
        self.assertEqual('job1', second_result['job']['job_id'])
        allure_result = call[2]['attachment']
        self.assertEqual('allure-report.tar', allure_result['filename'])
        self.assertEqual({}, allure_result['parent_step'])
        executionlog = call[3]['attachment']
        self.assertEqual('1970-01-01', executionlog['metadata']['creationTimestamp'])
        self.assertEqual({}, executionlog['job'])

    def test_list_attachments_with_hooks(self):
        mock_output = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._ensure_uuid',
            MagicMock(return_value=WORKFLOW_ID_EVENTS_OK),
        ), patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_WITH_HOOK),
        ), patch(
            'opentf.tools.ctlattachments.generate_output', mock_output
        ), patch(
            'opentf.tools.ctlattachments._file_exists', MagicMock(return_value=False)
        ):
            ctlattachments.list_attachments('wFiD', False)
        mock_output.assert_called_once()
        call = mock_output.call_args[0][0]
        self.assertEqual(2, len(call))
        hook = call[0]['attachment']
        self.assertEqual('bar.txt', hook['filename'])
        self.assertEqual(1, len(hook['parent_step']))
        self.assertEqual('setup', hook['parent_step']['sequence_id'])

    def test_list_attachments_check_testcase_data(self):
        workflow_id = WORKFLOW_ID_EVENTS_OK
        mock_output = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._ensure_uuid',
            MagicMock(return_value=workflow_id),
        ), patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_OK),
        ), patch(
            'opentf.tools.ctlattachments.generate_output', mock_output
        ):
            ctlattachments.list_attachments(workflow_id, False)
        mock_output.assert_called_once()
        call = mock_output.call_args[0][0]
        result_1 = call[0]['attachment']
        self.assertEqual(4, len(result_1['parent_step']))
        self.assertEqual('job1_step1', result_1['parent_step']['step_id'])
        self.assertEqual('smartTechnology', result_1['parent_step']['technology'])
        self.assertEqual(2, result_1['parent_step']['sequence_id'])
        result_2 = call[1]['attachment']
        self.assertEqual(4, len(result_2['parent_step']))
        self.assertEqual('unittest.st.st', result_2['parent_step']['testcase'])
        self.assertEqual(3, result_2['parent_step']['sequence_id'])

    def test_list_attachments_ko_and_verbose(self):
        mock_output = MagicMock()
        mock_print = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._ensure_uuid', MagicMock(return_value='WFID')
        ), patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_KO),
        ), patch(
            'opentf.tools.ctlattachments.generate_output', mock_output
        ), patch(
            'builtins.print', mock_print
        ), patch(
            'opentf.tools.ctlattachments._file_exists', MagicMock(return_value=False)
        ):
            ctlattachments.list_attachments('wFiD', True)
        mock_output.assert_called_once()
        mock_print.assert_called_once_with('Error-error\n\nError-error-error\n')

    def test_list_attachments_ko_not_exists_ko_exists(self):
        mock_output = MagicMock()
        mock_print = MagicMock()
        with patch(
            'opentf.tools.ctlattachments._ensure_uuid', MagicMock(return_value='WFID')
        ), patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_KO),
        ), patch(
            'opentf.tools.ctlattachments.generate_output', mock_output
        ), patch(
            'builtins.print', mock_print
        ), patch(
            'opentf.tools.ctlattachments._file_exists',
            MagicMock(side_effect=[True, False]),
        ):
            ctlattachments.list_attachments('wFiD', True)
        mock_output.assert_called_once()
        mock_print.assert_called_once_with('Error-error-error\n')

    # attachments commands

    def test_attachments_cmd_cp_ok(self):
        path = '/we/dont/need/no.destination'
        args = ['', 'rrrgs']
        mock_da = MagicMock()
        mock_eo = MagicMock(return_value=[f'{FIRST_UUID}:{SECOND_UUID}', path])
        mock_eu = MagicMock(side_effect=[FIRST_UUID, SECOND_UUID])
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlattachments.read_configuration', MagicMock()
        ), patch('opentf.tools.ctlattachments.download_attachment', mock_da), patch(
            'opentf.tools.ctlattachments._is_command', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlattachments._ensure_options', mock_eo
        ), patch(
            'opentf.tools.ctlattachments._ensure_uuid', mock_eu
        ):
            ctlattachments.attachments_cmd()
        self.assertEqual(2, mock_eu.call_count)
        mock_eo.assert_called_once_with('cp *', ['rrrgs'], extra=[('--type', '-t')])
        mock_da.assert_called_once_with(FIRST_UUID, SECOND_UUID, path)

    def test_attachments_cmd_get_attachments_ok(self):
        args = ['', 'get', 'attachments', WF_UUID]
        mock_eo = MagicMock(return_value='wOrkFloW_id')
        mock_la = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlattachments.read_configuration', MagicMock()
        ), patch(
            'opentf.tools.ctlattachments._is_command',
            MagicMock(side_effect=[False, True]),
        ), patch(
            'opentf.tools.ctlattachments._ensure_options', mock_eo
        ), patch(
            'opentf.tools.ctlattachments.list_attachments', mock_la
        ):
            ctlattachments.attachments_cmd()
        mock_eo.assert_called_once()
        mock_la.assert_called_once_with('wOrkFloW_id', False)

    def test_attachments_cmd_cp_with_pattern(self):
        path = '/we/dont/need/no.destination'
        args = ['', 'rrrgs']
        mock_da = MagicMock()
        mock_eo = MagicMock(return_value=[f'{FIRST_UUID}:*.txt', path])
        mock_eu = MagicMock(return_value=FIRST_UUID)
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlattachments.read_configuration', MagicMock()
        ), patch('opentf.tools.ctlattachments.download_attachments', mock_da), patch(
            'opentf.tools.ctlattachments._is_command', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctlattachments._ensure_options', mock_eo
        ), patch(
            'opentf.tools.ctlattachments._ensure_uuid', mock_eu
        ):
            ctlattachments.attachments_cmd()
        self.assertEqual(1, mock_eu.call_count)
        mock_eo.assert_called_once_with('cp *', ['rrrgs'], extra=[('--type', '-t')])
        mock_da.assert_called_once_with(FIRST_UUID, '*.txt', path)

    def test_attachments_cmd_cp_ko_missing_options(self):
        path = '/we/dont/need/no.destination'
        args = ['', 'rrrgs']
        mock_eo = MagicMock(return_value=[f':{SECOND_UUID}', path])
        mock_fatal = MagicMock(side_effect=SystemExit())
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlattachments._is_command', MagicMock(return_value=True)
        ), patch('opentf.tools.ctlattachments._ensure_options', mock_eo), patch(
            'opentf.tools.ctlattachments._fatal', mock_fatal
        ), patch(
            'opentf.tools.ctlattachments.read_configuration', MagicMock()
        ):
            self.assertRaises(SystemExit, ctlattachments.attachments_cmd)
        mock_fatal.assert_called_once_with(
            'Invalid parameters. Was expecting WORKFLOW_ID:ATTACHMENT_ID DESTINATION_FILEPATH, got: %s.',
            f':{SECOND_UUID} {path}',
        )

    def test_attachments_cmd_cp_ko_unknown_command(self):
        args = ['', 'rrrgs']
        mock_eo = MagicMock()
        mock_error = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctlattachments._is_command', MagicMock(return_value=False)
        ), patch('opentf.tools.ctlattachments._ensure_options', mock_eo), patch(
            'opentf.tools.ctlattachments._error', mock_error
        ):
            self.assertRaises(SystemExit, ctlattachments.attachments_cmd)
        mock_eo.assert_not_called()
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )

    # get attachment uuids

    def test_get_attachment_uuids_ok(self):
        with patch(
            'opentf.tools.ctlattachments._get_workflow_attachments_events',
            MagicMock(return_value=ATTACHMENT_EVENTS_OK),
        ):
            uuids = ctlattachments._get_attachment_uuids(WORKFLOW_ID_EVENTS_OK)
        self.assertEqual(4, len(uuids))
        self.assertIn('5b444910-a246-4d66-b02b-40e77148b90d', uuids)
        self.assertIn('b10a8635-0d36-4f40-8776-d9ac741e1b0f', uuids)

    # print attachments help

    def test_print_attachments_cp_help_ok(self):
        mock_ic = MagicMock(return_value=True)
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._is_command', mock_ic), patch(
            'opentf.tools.ctlattachments.print', mock_print
        ):
            ctlattachments.print_attachments_help(['a', 'b'])
        mock_ic.assert_called_once_with('cp', ['a', 'b'])
        mock_print.assert_called_once_with(ctlattachments.CP_HELP)

    def test_print_get_attachments_help_ok(self):
        mock_ic = MagicMock(side_effect=[False, True])
        mock_print = MagicMock()
        with patch('opentf.tools.ctlattachments._is_command', mock_ic), patch(
            'opentf.tools.ctlattachments.print', mock_print
        ):
            ctlattachments.print_attachments_help(['a', 'b'])
        mock_ic.assert_called_with('get attachments', ['a', 'b'])
        mock_print.assert_called_once_with(ctlattachments.GET_ATTACHMENTS_HELP)

    def test_print_attachments_help_ko_unknown_command(self):
        mock_ic = MagicMock(return_value=False)
        mock_print = MagicMock()
        mock_error = MagicMock()
        with patch('opentf.tools.ctlattachments._is_command', mock_ic), patch(
            'opentf.tools.ctlattachments.print', mock_print
        ), patch('opentf.tools.ctlattachments._error', mock_error):
            self.assertRaises(
                SystemExit, ctlattachments.print_attachments_help, ['a', 'b']
            )
        mock_print.assert_not_called()
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )
