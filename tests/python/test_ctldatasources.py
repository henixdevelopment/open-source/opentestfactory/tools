# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import unittest
import sys

from io import StringIO
from unittest.mock import MagicMock, patch

from opentf.tools import ctldatasources

########################################################################
# Helpers


def _make_response(status_code, json_value):
    mock = MagicMock()
    mock.status_code = status_code
    mock.json = lambda: json_value
    return mock


########################################################################


class TestCtlDatasources(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        self.held_output = StringIO()
        sys.stdout = self.held_output
        sys.stderr = self.held_output

    def tearDown(self):
        self.held_output.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # print help

    def test_print_get_datasources_help_ok(self):
        args = ['', 'rgrgrg']
        mock_print = MagicMock()
        with patch(
            'opentf.tools.ctldatasources._is_command', MagicMock(return_value=True)
        ), patch('builtins.print', mock_print):
            ctldatasources.print_get_datasources_help(args)
        mock_print.assert_called_once()

    def test_print_get_datasources_help_ko_unknown_command(self):
        args = ['', 'rgrgrg']
        mock_print = MagicMock()
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctldatasources._is_command', MagicMock(return_value=False)
        ), patch('builtins.print', mock_print), patch(
            'opentf.tools.ctldatasources._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctldatasources.print_get_datasources_help, args
            )
        mock_print.assert_not_called()
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )

    # get_datasources_cmd

    def test_get_datasources_cmd_ok(self):
        args = ['', 'get', 'wf_id', 'datasources']
        mock_gd = MagicMock()
        mock_eo = MagicMock(return_value='Wfff')
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctldatasources._ensure_options', mock_eo
        ), patch('opentf.tools.ctldatasources.read_configuration'), patch(
            'opentf.tools.ctldatasources._is_command', MagicMock(return_value=True)
        ), patch(
            'opentf.tools.ctldatasources.get_datasources', mock_gd
        ):
            ctldatasources.get_datasources_cmd()
        mock_eo.assert_called_once()
        mock_gd.assert_called_once_with('Wfff')

    def test_get_datasources_cmd_ko_unknown_command(self):
        args = ['', 'rrr']
        mock_error = MagicMock()
        with patch.object(sys, 'argv', args), patch(
            'opentf.tools.ctldatasources._is_command', MagicMock(return_value=False)
        ), patch('opentf.tools.ctldatasources._error', mock_error):
            self.assertRaisesRegex(SystemExit, '1', ctldatasources.get_datasources_cmd)
        mock_error.assert_called_once_with(
            'Unknown command.  Use --help to list known commands.'
        )

    # get_datasources

    def test_get_datasources_ok(self):
        mock_ds = MagicMock(return_value=('datasources', 'ONGOING'))
        mock_output = MagicMock()

        with patch(
            'opentf.tools.ctldatasources._ensure_uuid', MagicMock(return_value='W_F')
        ), patch(
            'opentf.tools.ctldatasources._get_arg',
            MagicMock(side_effect=['testcases', '23']),
        ), patch(
            'opentf.tools.ctldatasources._query_observer_datasources', mock_ds
        ), patch(
            'opentf.tools.ctldatasources.generate_output', mock_output
        ):
            ctldatasources.get_datasources('W_F')
        mock_ds.assert_called_once_with('W_F', 'testcases', 23)
        mock_output.assert_called_once_with(
            'datasources',
            ctldatasources.DEFAULT_COLUMNS_TESTCASES,
            ctldatasources.WIDE_COLUMNS_TESTCASES,
        )

    def test_get_datasources_no_kind(self):
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctldatasources._ensure_uuid', MagicMock(return_value='NoKind')
        ), patch(
            'opentf.tools.ctldatasources._get_arg', MagicMock(return_value=None)
        ), patch(
            'opentf.tools.ctldatasources._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctldatasources.get_datasources, 'NoKind'
            )
        mock_error.assert_called_once_with('No datasources kind provided.')

    def test_get_datasources_ko(self):
        mock_fatal = MagicMock()
        with patch(
            'opentf.tools.ctldatasources._ensure_uuid',
            MagicMock(return_value='Exception'),
        ), patch(
            'opentf.tools.ctldatasources._get_arg',
            MagicMock(return_value='testcases'),
        ), patch(
            'opentf.tools.ctldatasources._fatal', mock_fatal
        ), patch(
            'opentf.tools.ctldatasources._query_observer_datasources',
            MagicMock(side_effect=Exception('Well. That did not work.')),
        ):
            ctldatasources.get_datasources('Exception')
        mock_fatal.assert_called_once_with(
            'Failed to get %s datasource for workflow %s: %s.',
            'testcases',
            'Exception',
            'Well. That did not work.',
        )

    def test_get_datasources_ko_unknown_datasource(self):
        mock_error = MagicMock()
        with patch(
            'opentf.tools.ctldatasources._ensure_uuid',
            MagicMock(return_value='UnknownKind'),
        ), patch(
            'opentf.tools.ctldatasources._get_arg', MagicMock(return_value='foo')
        ), patch(
            'opentf.tools.ctldatasources._error', mock_error
        ):
            self.assertRaisesRegex(
                SystemExit, '1', ctldatasources.get_datasources, 'UnknownKind'
            )
        mock_error.assert_called_once_with(
            'Unknown datasource, was expecting %s.',
            ', '.join(ctldatasources.DATASOURCES_COLUMNS.keys()),
        )

    # _query_observer_datasources

    def test_query_observer_datasources_ok(self):
        mockresponse = _make_response(
            200, {'details': {'items': [{'a': 'b'}, {'c': 'd'}]}}
        )
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ):
            result = ctldatasources._query_observer_datasources('wff', 'testcases', 8)
        mock_get.assert_called_once()
        self.assertEqual(2, len(result))
        self.assertEqual({'a': 'b'}, result[0][0])

    def test_query_observer_datasources_paginated_response(self):
        mockresponse = _make_response(
            200, {'details': {'items': [{'e': 'f'}, {'g': 'h'}]}}
        )
        mockresponse.links = {'next': {'url': 'foo?bar'}}
        mockresponse_2 = _make_response(200, {'details': {'items': [{'i': 'j'}]}})
        mock_get = MagicMock(side_effect=[mockresponse, mockresponse_2])
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ):
            result = ctldatasources._query_observer_datasources('wff', 'testcases', 8)
        self.assertEqual(2, mock_get.call_count)
        self.assertEqual(3, len(result[0]))

    def test_query_observer_datasources_workflow_still_running(self):
        mockresponse = _make_response(
            200, {'message': 'Still Running', 'details': {'items': []}}
        )
        mock_get = MagicMock(return_value=mockresponse)
        mock_print = MagicMock()
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ), patch('builtins.print', mock_print):
            self.assertRaisesRegex(
                SystemExit,
                '0',
                ctldatasources._query_observer_datasources,
                'wff',
                'testcases',
                8,
            )
        mock_get.assert_called_once()
        mock_print.assert_called_once_with('Still Running')

    def test_query_observer_datasources_no_results(self):
        mockresponse = _make_response(200, {'message': '', 'details': {'items': []}})
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ):
            res = ctldatasources._query_observer_datasources('wf', 'testcases', 8)
            self.assertEqual(([], 'UNKNOWN'), res)

    def test_query_observer_datasources_202_then_200_legacy(self):
        mockresponse = _make_response(202, {'details': {'status': 'RUNNING'}})
        mockresponse_2 = _make_response(
            200, {'details': {'status': 'DONE', 'items': [{'0': '1'}]}}
        )
        mock_get = MagicMock(side_effect=[mockresponse, mockresponse_2])
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ), patch('time.sleep') as mock_sleep:
            result = ctldatasources._query_observer_datasources('wfff', 'testcases', 8)
        mock_sleep.assert_called_once()
        self.assertEqual(2, len(result))
        self.assertEqual([{'0': '1'}], result[0])

    def test_query_observer_datasources_202_then_timeout_legacy(self):
        mockresponse = _make_response(202, {'details': {'status': 'RUNNING'}})
        mock_get = MagicMock(return_value=mockresponse)
        mock_time = MagicMock(side_effect=[1, 10, 20, 30])
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ), patch('time.sleep'), patch('time.time', mock_time), patch(
            'builtins.print'
        ) as mock_print:
            self.assertRaisesRegex(
                SystemExit,
                '101',
                ctldatasources._query_observer_datasources,
                'wfff',
                'testcases',
                8,
            )
        mock_print.assert_called_once_with(
            'Datasource events caching timeout (8 sec), aborting.'
        )

    def test_query_observer_datasources_202(self):
        mockresponse = _make_response(202, {'details': {'status': 'ONGOING'}})
        mock_get = MagicMock(return_value=mockresponse)
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ), patch('builtins.print') as mock_print:
            result = ctldatasources._query_observer_datasources('wfff', 'testcases', 8)
        self.assertEqual(([], 'ONGOING'), result)

    def test_query_observer_datasources_ve(self):
        mockresponse = _make_response(
            200, {'details': {'items': [{'e': 'f'}, {'g': 'h'}]}}
        )
        mockresponse.links = {'next': {'url': 'foo?bar'}}
        mock_get = MagicMock(side_effect=[mockresponse, ValueError('Bad value, bad')])
        mock_error = MagicMock()
        mock_debug = MagicMock()
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer', MagicMock()
        ), patch('opentf.tools.ctldatasources._error', mock_error), patch(
            'opentf.tools.ctldatasources._debug', mock_debug
        ):
            self.assertRaisesRegex(
                SystemExit,
                '2',
                ctldatasources._query_observer_datasources,
                'wff',
                'testcases',
                8,
            )
        self.assertEqual(2, mock_get.call_count)
        mock_error.assert_called_once_with(
            'Could not deserialize observer response: %s.', 'Bad value, bad'
        )
        mock_debug.assert_called_once()

    def test_query_observer_datasources_response_422(self):
        mockresponse = _make_response(
            422, {'message': 'Scope error occurred', 'details': {'items': []}}
        )
        mock_get = MagicMock(return_value=mockresponse)
        mock_error = MagicMock()
        with patch('opentf.tools.ctldatasources._get', mock_get), patch(
            'opentf.tools.ctldatasources._observer'
        ), patch('opentf.tools.ctldatasources._error', mock_error):
            self.assertRaisesRegex(
                SystemExit,
                '1',
                ctldatasources._query_observer_datasources,
                'wff',
                'testcases',
                8,
            )
        mock_get.assert_called_once()
        mock_error.assert_called_once_with('Scope error occurred')

    def test_get_timeout_ve(self):
        with patch(
            'opentf.tools.ctldatasources._get_arg', MagicMock(return_value='-43')
        ), patch('opentf.tools.ctldatasources._warning') as mock_warn:
            res = ctldatasources._get_timeout()
        mock_warn.assert_called_once_with(
            'Timeout must be a positive integer, got %s, resetting to default value.',
            '-43',
        )
        self.assertEqual(ctldatasources.DATASOURCES_TIMEOUT, res)
